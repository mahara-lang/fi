<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['completeregistration'] = 'Täydellinen rekisteröinti';

$string['description'] = 'Sisäinen autentikointi';

$string['emailalreadytaken'] = 'Tämä sähköpostiosoite on jo käytössä';

$string['iagreetothetermsandconditions'] = 'Hyväksyn käyttöehdot';

$string['internal'] = 'Sisäinen';

$string['passwordformdescription'] = 'Salasanasi täytyy olla vähintään kuusi merkkiä pitkä ja sen tulee sisältää vähintään yhden numeron ja kaksi kirjainta';

$string['passwordinvalidform'] = 'Salasanasi täytyy olla vähintään kuusi merkkiä pitkä ja sen tulee sisältää vähintään yhden numeron ja kaksi kirjainta';

$string['registeredemailmessagehtml'] = '<p>Hei %s,</p> <p>Kiitos, että rekisteröit käyttäjätilin %s. Ole hyvä ja klikkaa tätä linkkiä suorittaaksesi loppuun sisäänkirjautumisprosessin: </p> <p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p> <p>Linkki erääntyy 24 tunnin kuluessa.</p> <pre>-- Terveisin, %s Tiimi</pre>';

$string['registeredemailmessagetext'] = 'Hei%s, Kiitos, että rekisteröit käyttäjätilin %s. Ole hyvä ja klikkaa tätä linkkiä suorittaaksesi loppuun sisäänkirjautumisprosessin: %sregister.php?key=%s Linkki erääntyy 24 tunnin kuluessa. -- Terveisin, %s Tiimi';

$string['registeredemailsubject'] = 'Olet rekisteröitynyt palveluun %s';

$string['registeredok'] = '<p>Rekisteröitymisesi on onnistunut. Sähköpostiisi on lähetetty ohjeet kuinka aktivoit käyttäjätilisi</p>';

$string['registrationnosuchkey'] = 'Valitettavasti näyttää siltä, ettei tällä avaimella ole rekisteröintiä. Ehkä odotit yli 24 tuntia ennen kuin suoritit loppuun rekisteröintisi? Muussa tapauksessa, syy voi olla meidän.';

$string['registrationunsuccessful'] = 'Valitettavasti rekisteröintiyrityksesi epäonnistui. Tämän on meidän virhe, ei sinun. Ole hyvä ja yritä uudestaan.';

$string['title'] = 'Sisäinen';

$string['usernamealreadytaken'] = 'Valitettavasti tämä käyttäjätunnus on jo käytössä';

$string['usernameinvalidadminform'] = 'Käyttäjänimi voi sisältää kirjaimia, numeroita ja useimpia muita merkkejä. Sen tulee olla vähintään kolme ja enintään 236 merkkiä pitkä. Välilyönnit eivät ole sallittuja.';

$string['usernameinvalidform'] = 'Käyttäjätunnus voi sisältää kirjaimia, numeroita ja yleisempiä symboleja ja sen täytyy olla 3-30 merkin mittainen. Välilyönnit eivät ole sallittuja.';

$string['youmaynotregisterwithouttandc'] = 'Et voi rekisteröityä ellet hyväksy <a href="terms.php">Käyttöehdot</a>';

