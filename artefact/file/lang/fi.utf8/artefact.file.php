<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['Contents'] = 'Sisällöt';

$string['Continue'] = 'Jatka';

$string['Created'] = 'Luotu';

$string['Date'] = 'Päivämäärä';

$string['Default'] = 'Oletuksena';

$string['Description'] = 'Kuvaus';

$string['Details'] = 'Tarkemmat tiedot';

$string['Download'] = 'Lataa koneellesi';

$string['Files'] = 'Tiedostot';

$string['Folder'] = 'Kansio';

$string['Folders'] = 'Kansiot';

$string['Images'] = 'Kuvat';

$string['Name'] = 'Nimi';

$string['Owner'] = 'Omistaja';

$string['Preview'] = 'Esikatselu';

$string['Size'] = 'Koko';

$string['Title'] = 'Otsikko';

$string['Type'] = 'Tyyppi';

$string['Unzip'] = 'Pura zip-tiedosto';

$string['addafile'] = 'Lisää tiedosto';

$string['ai'] = 'Postscript-dokumentti';

$string['aiff'] = 'AIFF-äänitiedosto';

$string['application'] = 'Tuntematon sovellus';

$string['archive'] = 'Arkisto';

$string['au'] = 'AU-äänitiedosto';

$string['avi'] = 'AVI-videotiedosto';

$string['bmp'] = 'Bitmap-kuva';

$string['bytes'] = 'tavua';

$string['bz2'] = 'Bzip2-pakattu tiedosto';

$string['cannoteditfolder'] = 'Sinulla ei ole oikeutta lisätä sisältöä tähän kansioon';

$string['cannoteditfoldersubmitted'] = 'Et voi lisätä sisältöä kansioon, joka on palautetulla sivulla.';

$string['cannotremovefromsubmittedfolder'] = 'Et voi poistaa sisältöä kansiosta, joka on palautetulla sivulla.';

$string['cantcreatetempprofileiconfile'] = 'Profiilikuvan väliaikaistiedostoa ei voi kirjoittaa kohteeseen %s';

$string['changessaved'] = 'Muutokset tallennettu';

$string['clickanddragtomovefile'] = 'Klikkaa ja vedä %s';

$string['confirmdeletefile'] = 'Haluatko varmasti poistaa tämän tiedoston?';

$string['confirmdeletefolder'] = 'Haluatko varmasti poistaa tämän kansion?';

$string['confirmdeletefolderandcontents'] = 'Haluatko varmasti poistaa tämän kansion ja koko sen sisällön?';

$string['contents'] = 'Kansion sisältö';

$string['copyrightnotice'] = 'Tekijänoikeusilmoitus';

$string['create'] = 'Luo';

$string['createfolder'] = 'Luo kansio';

$string['customagreement'] = 'Mukautetut käyttöehdot';

$string['defaultagreement'] = 'Oletuskäyttöehdot';

$string['defaultquota'] = 'Oletuskiintiö';

$string['defaultquotadescription'] = 'Voit asettaa uusille käyttäjille heidän kiintiönsä levytilasta tässä. Nykyisten käyttäjien kiintiö ei muutu.';

$string['deletefile?'] = 'Haluatko varmasti poistaa tämän tiedoston?';

$string['deletefolder?'] = 'Haluatko varmasti poistaa tämän kansion?';

$string['deleteselectedicons'] = 'Poista valitut profiilikuvat';

$string['deletingfailed'] = 'Poistaminen epäonnistui: tiedostoa tai kansiota ei enää ole';

$string['destination'] = 'Kohde';

$string['doc'] = 'Word-dokumentti';

$string['downloadfile'] = 'Tallenna %s koneellesi';

$string['downloadoriginalversion'] = 'Tallenna dokumentin alkuperäinen versio koneellesi';

$string['dss'] = 'Digital Speech Standard -äänitiedosto';

$string['editfile'] = 'Muokkaa tiedostoa';

$string['editfolder'] = 'Muokkaa kansiota';

$string['editingfailed'] = 'Muokkaus epäonnistui: tiedostoa tai kansiota ei enää ole';

$string['emptyfolder'] = 'Tyhjä kansio';

$string['extractfilessuccess'] = 'Luotu %s kansio(ta) ja %s tiedosto(a)';

$string['file'] = 'Tiedosto';

$string['fileadded'] = 'Tiedosto valittu';

$string['filealreadyindestination'] = 'Siirtämäsi tiedosto on jo tässä kansiossa';

$string['fileappearsinviews'] = 'Tätä tiedostoa käytetään yhdellä tai useammalla sivulla.';

$string['fileattached'] = 'Tiedosto on liitetty %s:n kohtaan portfoliossasi.';

$string['fileexists'] = 'Tiedosto on olemassa';

$string['fileexistsoverwritecancel'] = 'Tiedosto tällä nimellä on jo olemassa. Voit yrittää eri nimeä tai tallentaa nykyisen tiedoston päälle.';

$string['fileinstructions'] = 'Lataa kuvia, dokumentteja tai muita tiedostoja. Käytä raahaus-toimintoa siirtääksesi tiedostoja kansiosta toiseen.';

$string['filelistloaded'] = 'Tiedostolista ladattu';

$string['filemoved'] = 'Tiedosto siirretty';

$string['filenamefieldisrequired'] = 'Tiedostokenttä vaaditaan';

$string['filepermission.edit'] = 'Muokkaa';

$string['filepermission.republish'] = 'Julkaise';

$string['filepermission.view'] = 'Näytä';

$string['fileremoved'] = 'Tiedosto poistettu';

$string['files'] = 'tiedostot';

$string['filesextractedfromarchive'] = 'Tiedostot, jotka on purettu arkistosta';

$string['filesextractedfromziparchive'] = 'Tiedostot, jotka on purettu zip-arkistosta';

$string['fileswillbeextractedintofolder'] = 'Tiedostot puretaan kansioon %s';

$string['filethingdeleted'] = '%s poistettu';

$string['fileuploadedas'] = '%s ladattu nimellä "%s"';

$string['fileuploadedtofolderas'] = '%s ladattu kansioon %s nimellä "%s"';

$string['filewithnameexists'] = 'Tiedosto tai kansio nimellä "%s" on jo olemassa.';

$string['flv'] = 'FLV Flash -elokuva';

$string['folder'] = 'Kansio';

$string['folderappearsinviews'] = 'Tätä kansiota käytetään yhdellä tai useammalla sivulla.';

$string['foldercreated'] = 'Kansio luotu';

$string['foldernamerequired'] = 'Anna nimi uudelle kansiolle';

$string['foldernotempty'] = 'Tämä kansio ei ole tyhjä.';

$string['gif'] = 'GIF-kuva';

$string['gotofolder'] = 'Mene kansioon %s';

$string['groupfiles'] = 'Ryhmätiedostot';

$string['gz'] = 'Gzip Compressed -tiedosto';

$string['home'] = 'Pääkansio';

$string['html'] = 'HTML-tiedosto';

$string['htmlremovedmessage'] = 'Katsot käyttäjän <a href="%2$s">%3$s</a> tiedostoa <strong>%1$s</strong>. Mahdolliset haitalliset osat on poistettu, joten tiedosto ei välttämättä vastaa alkuperäistä.';

$string['htmlremovedmessagenoowner'] = 'Katsot tiedostoa <strong>%s</strong>. Mahdolliset haitalliset osat on poistettu, joten tiedosto ei välttämättä vastaa alkuperäistä.';

$string['image'] = 'Kuva';

$string['imagetitle'] = 'Kuvan otsikko';

$string['insufficientquotaforunzip'] = 'Tiedostokiintiösi on liian pieni purkaaksesi tämän tiedoston.';

$string['invalidarchive'] = 'Virheilmoitus arkistotiedoston lukemisessa.';

$string['jpeg'] = 'JPEG-kuva';

$string['jpg'] = 'JPEG-kuva';

$string['js'] = 'Javascript-tiedosto';

$string['lastmodified'] = 'Viimeksi muokattu';

$string['latex'] = 'LaTeX-dokumentti';

$string['m3u'] = 'M3U-äänitiedosto';

$string['maxquota'] = 'Maksimikiintiö';

$string['maxquotadescription'] = 'Voit asettaa maksimikiintiön, joka käyttäjälle voidaan antaa. Muutos ei vaikuta olemassa oleviin kiintiöihin';

$string['maxquotaenabled'] = 'Käytä palvelun laajuista maksimikiintiötä';

$string['maxquotaexceeded'] = 'Kiintiö on suurempi kuin palvelun suurin sallittu (%s). Määrittele pienmpi arvo tai ota yhteyttä ylläpitoon kiintiön nostamiseksi.';

$string['maxquotaexceededform'] = 'Määrittele kiintiö, joka on pienempi kuin %s.';

$string['maxquotatoolow'] = 'Maksimikiintiö ei voi olla pienempi kuin oletuskiintiö';

$string['maxuploadsize'] = 'Maksimikoko lataamiselle';

$string['mov'] = 'MOV Quicktime -elokuva';

$string['movefailed'] = 'Siirto epäonnistui.';

$string['movefaileddestinationinartefact'] = 'Et voi laittaa kansiota itsensä sisään.';

$string['movefaileddestinationnotfolder'] = 'Voit siirtää tiedostoja ainoastaan kansioihin.';

$string['movefailednotfileartefact'] = 'Vain tiedosto-, kansio- ja kuvatuotokset voidaan siirtää.';

$string['movefailednotowner'] = 'Sinulla ei ole oikeuksia siirtää tiedostoa tähän kansioon';

$string['movingfailed'] = 'Siirto epäonnistui: tiedostoa tai kansiota ei enää ole';

$string['mp3'] = 'MP3-äänitiedosto';

$string['mp4_audio'] = 'MP4-äänitiedosto';

$string['mp4_video'] = 'MP4-videotiedosto';

$string['mpeg'] = 'MPEG-elokuva';

$string['mpg'] = 'MPG-elokuva';

$string['myfiles'] = 'Omat tiedostot';

$string['namefieldisrequired'] = 'Nimikenttä on pakollinen';

$string['nametoolong'] = 'Tämä nimi on liian pitkä. Ole hyvä ja valitse lyhyempi.';

$string['nofilesfound'] = 'Tiedostoja ei löytynyt';

$string['noimagesfound'] = 'Kuvia ei löytynyt';

$string['notpublishable'] = 'Sinulla ei ole oikeuksia julkaista tätä tiedostoa';

$string['odb'] = 'Openoffice-tietokanta';

$string['odc'] = 'Openoffice Calc -tiedosto';

$string['odf'] = 'Openoffice Formula -tiedosto';

$string['odg'] = 'Openoffice Graphics -tiedosto';

$string['odi'] = 'Openoffice-kuva';

$string['odm'] = 'Openoffice Master Document -tiedosto';

$string['odp'] = 'Openoffice Presentation';

$string['ods'] = 'Openoffice Spreadsheet';

$string['odt'] = 'Openoffice-dokumentti';

$string['onlyfiveprofileicons'] = 'Voit ladata korkeintaan viisi profiilikuvaa';

$string['or'] = 'tai';

$string['oth'] = 'Openoffice Web -dokumentti';

$string['ott'] = 'Openoffice Template -dokumentti';

$string['overwrite'] = 'Päällekirjoittaa';

$string['parentfolder'] = 'Yläkansio';

$string['pdf'] = 'PDF-dokumentti';

$string['pleasewaitwhileyourfilesarebeingunzipped'] = 'Odota kunnes tiedostosi on purettu.';

$string['pluginname'] = 'Tiedostot';

$string['png'] = 'PNG-kuva';

$string['ppt'] = 'MS Powerpoint -dokumentti';

$string['profileicon'] = 'Profiilikuvat';

$string['profileiconimagetoobig'] = 'Kuva, jonka latasit on liian iso (%sx%s pikseliä). Kuva saa olla korkeintaan %sx%s pikseliä';

$string['profileicons'] = 'Profiilikuvat';

$string['profileiconsdefaultsetsuccessfully'] = 'Profiilikuva asetettu';

$string['profileiconsdeletedsuccessfully'] = 'Profiilikuva(t) poistettu';

$string['profileiconsetdefaultnotvalid'] = 'Profiilikuvan asetetus epäonnistui. Valinta on virheellinen.';

$string['profileiconsiconsizenotice'] = 'Voit ladata<strong> viisi</strong> profiilikuvaa ja valita niistä yhden, jota käytetään oletuskuvana. Yksittäisen kuvan koon tulee olla 16x16 ja %sx%s pikselin välillä.';

$string['profileiconsize'] = 'Profiilikuvan koko';

$string['profileiconsnoneselected'] = 'Profiilikuvia ei valittu poistettavaksi';

$string['profileiconuploadexceedsquota'] = 'Tämän profiilikuvan lataaminen ylittää levykiintiösi. Poista jokin aiemmin lisätty tiedosto';

$string['quicktime'] = 'Quicktime-elokuva';

$string['ra'] = 'Real-äänitiedosto';

$string['ram'] = 'RAM Real Player -elokuva';

$string['removingfailed'] = 'Poistaminen epäonnistui: tiedostoa tai kansiota ei enää ole';

$string['requireagreement'] = 'Käyttöehtojen hyväksyminen vaaditaan';

$string['rm'] = 'RM Real Player -elokuva';

$string['rpm'] = 'RPM Real Player -elokuva';

$string['rtf'] = 'RTF-dokumentti';

$string['savechanges'] = 'Tallenna muutokset';

$string['selectafile'] = 'Valitse tiedosto';

$string['selectingfailed'] = 'Valinta epäonnistui: tiedostoa tai kansiota ei enää ole';

$string['setdefault'] = 'Aseta oletukseksi';

$string['sgi_movie'] = 'SGI-elokuvatiedosto';

$string['sh'] = 'Sh-skriptitiedosto';

$string['sitefilesloaded'] = 'www-sivujen tiedostot ladattu';

$string['spacerequired'] = 'Vaadittu tila';

$string['spaceused'] = 'Tilaa käytetty';

$string['swf'] = 'SWF Flash -elokuva';

$string['tar'] = 'TAR-arkisto';

$string['timeouterror'] = 'Tiedoston lataaminen epäonnistui: yritä uudelleen lataamalla tiedosto uudestaan';

$string['title'] = 'Nimi';

$string['titlefieldisrequired'] = 'Nimi vaaditaan';

$string['txt'] = 'Tekstitiedosto';

$string['unzipprogress'] = '%s tiedostoa/kansiota luotu.';

$string['upload'] = 'Lataa';

$string['uploadagreement'] = 'Tiedoston lisäyssitoumus';

$string['uploadagreementdescription'] = 'Salli tämä asetus jos haluat pakottaa käyttäjät suostumaan allaolevaan tekstiin, ennen kuin he voivat ladata tiedoston tälle sivustolle.';

$string['uploadedprofileiconsuccessfully'] = 'Uusi profiilikuva ladattu';

$string['uploadexceedsquota'] = 'Tämän tiedoston lataaminen ylittää levykiintiösi. Yritä poistamalla joitakin tiedostoja, jotka olet ladannut aiemmin.';

$string['uploadfile'] = 'Lisää tiedosto';

$string['uploadfileexistsoverwritecancel'] = 'Tiedosto tällä nimellä on jo olemassa. Voit nimetä uudestaan tiedoston, jota olet lataamassa tai voit tallentaa nykyisen tiedoston päälle.';

$string['uploadingfile'] = 'lisätään tiedostoa...';

$string['uploadingfiletofolder'] = 'Ladataan tiedostoa %s kansioon %s';

$string['uploadoffilecomplete'] = '%s lataus valmis';

$string['uploadoffilefailed'] = '%s lataus epäonnistui';

$string['uploadoffiletofoldercomplete'] = 'Tiedoston %s lataus kansioon %s valmis';

$string['uploadoffiletofolderfailed'] = 'Tiedoston %s lataus kansioon %s epäonnistui';

$string['uploadprofileicon'] = 'Lataa profiilikuva';

$string['usecustomagreement'] = 'Käytä mukautettuja käyttöehtoja';

$string['usenodefault'] = 'Ei oletuskuvaa';

$string['usingnodefaultprofileicon'] = 'Profiilikuva poistettiin käytöstä';

$string['wav'] = 'WAV-äänitiedosto';

$string['wmv'] = 'WMV-videotiedosto';

$string['wrongfiletypeforblock'] = 'Lataamasi tiedosto ei ole oikeaa tyyppiä tähän alueeseen.';

$string['xml'] = 'XML-tiedosto';

$string['youmustagreetothecopyrightnotice'] = 'Sinun täytyy hyväksyä tekijänoikeusilmoitus';

$string['zip'] = 'ZIP-arkisto';

