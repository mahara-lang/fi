<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['Add'] = 'Lisää';

$string['Admins'] = 'Pääkäyttäjät';

$string['Advanced'] = 'Laaja';

$string['Close'] = 'Sulje';

$string['Cron'] = 'Cron';

$string['Everyone'] = 'Kaikki';

$string['Extensions'] = 'Laajennokset';

$string['Field'] = 'Kenttä';

$string['Import'] = 'Tuo';

$string['Institution'] = 'Instituutio';

$string['Lockedfields'] = 'Lukitut kentät';

$string['Loggedin'] = 'Kirjautuneita';

$string['Maximum'] = 'Maksimi';

$string['Members'] = 'Jäsenet';

$string['Non-members'] = 'Ei-jäsenet';

$string['None'] = 'Ei mitään';

$string['Open'] = 'Avaa';

$string['Or...'] = 'Tai...';

$string['Plugin'] = 'Plugin';

$string['Register'] = 'Rekisteröidy';

$string['Search'] = 'Etsi';

$string['Simple'] = 'Suppea';

$string['Staff'] = 'Ylläpitäjät';

$string['Value'] = 'Arvo';

$string['about'] = 'Palvelusta';

$string['accountexpiry'] = 'Käyttäjätili erääntyy';

$string['accountexpirydescription'] = 'Käyttäjätilin erääntymispäivä';

$string['accountsettings'] = 'Käyttäjätilin asetukset';

$string['accountsettingslegend'] = 'Tilien asetukset';

$string['activeusers'] = 'Aktiivisia käyttäjiä';

$string['addcategories'] = 'Lisää kategorioita';

$string['addinstitution'] = 'Lisää instituutio';

$string['addmembers'] = 'Lisää jäseniä';

$string['addnewmembers'] = 'Lisää uusia jäseniä';

$string['adduser'] = 'Lisää käyttäjä';

$string['adduserdescription'] = 'Luo uusi käyttäjä';

$string['addusertoinstitution'] = 'Lisää käyttäjä instituutioon';

$string['adminauthorities'] = 'Oikeuksien hallinnointi';

$string['adminfilespagedescription'] = 'Voit lisätä tiedostoja liitettäväksi Linkit ja resurssit -valikkoon. Kotikansiossa olevat tiedostot voidaan näyttää kirjautuneille käyttäjille ja julkisessa kansiossa olevat kaikille.';

$string['adminhome'] = 'Pääkäyttäjän sivu';

$string['admininstitutions'] = 'Instituutiohallinta';

$string['administergroups'] = 'Hallitse ryhmiä';

$string['administergroupsdescription'] = 'Nimitä ryhmien pääkäyttäjiä ja poista ryhmiä';

$string['administration'] = 'Hallinta';

$string['adminnoauthpluginforinstitution'] = 'Ole hyvä ja konfiguroi tunnistuspluginit tälle instituutiolle.';

$string['adminnotifications'] = 'Pääkäyttäjän ilmoitukset';

$string['adminnotificationsdescription'] = 'Yhteenveto kuinka hallinnoijat saavat järjestelmäilmoituksia';

$string['adminpublicdirname'] = 'julkinen';

$string['adminsandstaffonly'] = 'Vain pääkäyttäjät ja ylläpitäjät';

$string['adminsonly'] = 'Vain pääkäyttäjät';

$string['adminusers'] = 'Palvelun pääkäyttäjät';

$string['adminusersdescription'] = 'Valitse palveluntarjoajan pääsyoikeudet';

$string['adminuserspagedescription'] = '<p>Tästä voi valita ketkä käyttäjät ovat tämän sivuston pääkäyttäjiä. Nykyiset pääkäyttäjät on listattu oikealle ja mahdolliset pääkäyttäjät vasemmalle.</p><p>Palvelussa täytyy olla vähintään yksi pääkäyttäjä.</p>';

$string['adminusersupdated'] = 'Pääkäyttäjien lista päivitetty';

$string['advanced'] = 'Laajennettu';

$string['allowgroupcategories'] = 'Salli ryhmien kategoriat';

$string['allowgroupcategoriesdescription'] = 'Jos valittu, pääkäyttäjät voivat luoda kategorioita, joihin käyttäjät voivat liittää ryhmiä';

$string['allowmobileuploads'] = 'Salli tiedostojen siirto mobiilikäyttöliittymällä';

$string['allowmobileuploadsdescription'] = 'Jos valittu, käyttäjät voivat siirtää tiedostoja Android-puhelimille tehdyllä sovelluksella';

$string['allowpublicprofiles'] = 'Salli julkiset profiilit';

$string['allowpublicprofilesdescription'] = 'Mikäli asetettu, käyttäjät voivat näyttää profiilinsa myös kirjautumattomille käyttäjille';

$string['allowpublicviews'] = 'Salli julkiset sivut';

$string['allowpublicviewsdescription'] = 'Mikäli asetettu, käyttäjät voivat luoda portfoliosivuja, jotka ovat näkyvissä myös kirjautumattomille käyttäjille';

$string['anonymouscomments'] = 'Nimettömät kommentit';

$string['anonymouscommentsdescription'] = 'Jos valittu, kirjautumattomat käyttäjät voivat kommentoida näkymiä.';

$string['antispam'] = 'Roskapostin torjunta';

$string['antispamdescription'] = 'Julkisilla lomakkeilla käytettävät roskapostin torjuntatavat';

$string['authenticatedby'] = 'Tunnistamistapa';

$string['authenticatedbydescription'] = 'Kuinka tämä käyttäjä tunnistautuu Maharaan';

$string['authplugin'] = 'Tunnistautumisplugin';

$string['badmenuitemtype'] = 'Tuntematon tiedostotyyppi';

$string['basicdetails'] = '2 - Perustiedot';

$string['basicinformationforthisuser'] = 'Perustiedot tästä käyttäjästä';

$string['becomeadminagain'] = 'Palaa käyttäjäksi %s';

$string['blockcountsbytype'] = 'Eniten käytettyjä lohkoja portfoliosivuilla';

$string['bulkexport'] = 'Vie käyttäjiä';

$string['bulkexportdescription'] = 'valitse instituutio, jonka käyttäjät haluat viedä <b>TAI</b> syötä lista käyttäjänimistä:';

$string['bulkexportempty'] = 'Ei vietäviä käyttäjiä. Tarkista käyttäjänimilista.';

$string['bulkexportinstitution'] = 'Instituutio, jonka käyttäjät viedään';

$string['bulkexporttitle'] = 'Vie käyttäjiä Leap2A-tiedostoihin';

$string['bulkexportusernames'] = 'Vietävät käyttäjänimet';

$string['bulkexportusernamesdescription'] = 'Lista käyttäjistä (yksi käyttäjätunnus/rivi), joiden tiedot viedään';

$string['bulkimportdirdoesntexist'] = 'Hakemistoa %s ei ole olemassa';

$string['bulkleap2aimport'] = 'Tuo käyttäjiä Leap2A-tiedostoista';

$string['bulkleap2aimportdescription'] = '<p>Voit tuoda joukon käyttäjiä palvelimella olevista Leap2A-tiedostoista. Määrittele zip-tiedoston sijainti, joka sisältää kaikki Leap2A-tiedostot. Tämän lisäksi tarvitaan yksi usernames.csv-niminen tiedosto, joka yhdistää käyttäjä- ja tiedostonimet.</p>
<p>usernames.csv näyttää tältä:</p>
<pre>
&nbsp;&nbsp;bob,mahara-export-leap-user8-1265165366.zip<br>
&nbsp;&nbsp;nigel,mahara-export-leap-user1-1266458159.zip
</pre>
<p>jossa mahara-export-leap-user8-1265165366.zip ja mahara-export-leap-user1-1266458159.zip ovat tiedostoja alihakemistossa users.</p>
<p>Tämä zip-tiedosto tulisi generoida Maharan käyttäjienvientitoiminnolla.</p>
<p>Jos olet tuomassa paljon käyttäjiä, ole kärsivällinen. Toiminto saattaa kestää kauan.</p>';

$string['bulkleap2aimportfiledescription'] = 'Zip-tiedosto palvelimella, joka sisältää Leap2A-tiedostot kaikista tuotavista käyttäjistä sekä csv-tiedoston käyttäjänimistä';

$string['changeinstitution'] = 'Vaihda instituutio';

$string['changeusername'] = 'Change username';

$string['changeusernamedescription'] = 'Change this user\'s username. Usernames are 3-236 characters long, and may contain letters, numbers, and most common symbols excluding spaces.';

$string['clickthebuttontocreatetheuser'] = 'Klikkaa painiketta perustaaksesi käyttäjän';

$string['closesite'] = 'Sulje palvelu';

$string['closesitedetail'] = 'Voit sulkea palvelun kaikilta muilta paitsi pääkäyttäjiltä. Tämä on hyödyllistä silloin kun valmistellaan tietokantapäivitystä. Vain pääkäyttäjillä on mahdollisuus kirjautua sisään kunnes avaat palvelun uudestaan tai päivitys on saatu päätökseen onnistuneesti.';

$string['component'] = 'Komponentti tai plugin';

$string['configextensions'] = 'Laajennukset';

$string['configsite'] = 'Palvelun asetukset';

$string['configureauthplugin'] = 'Sinun tulee määritellä tunnistautumisplugin ennen kuin voit lisätä käyttäjiä';

$string['configusers'] = 'Käyttäjähallinta';

$string['confirm'] = 'vahvista';

$string['confirmdeletecategory'] = 'Haluatko varmasti poistaa kategorian?';

$string['confirmdeletemenuitem'] = 'Haluatko varmasti poistaa tämän? (this item - admin.php)';

$string['confirmdeleteuser'] = 'Haluatko varmasti poistaa tämän käyttäjän?';

$string['confirmdeleteusers'] = 'Haluatko varmasti poistaa valitut käyttäjät?';

$string['confirmremoveuserfrominstitution'] = 'Haluatko varmasti poistaa käyttäjän tästä instituutiosta?';

$string['continue'] = 'Jatka';

$string['copyright'] = 'Copyright &copy; 2006 onwards, <a href="http://wiki.mahara.org/Contributors">Catalyst IT Ltd and others</a>' /** MISSING **/ ;

$string['coredata'] = 'Perustiedot';

$string['coredatasuccess'] = 'Perustiedot asennettiin onnistuneesti';

$string['couldnotexportusers'] = 'Seuraavia käyttäjiä ei voitu viedä: %s';

$string['country'] = 'Maa';

$string['create'] = '3 - Luo';

$string['createnewuserfromscratch'] = 'Luo uusi käyttäjä tyhjästä';

$string['createuser'] = 'Luo käyttäjä';

$string['cronnotrunning'] = 'Cron ei ole toiminnassa.<br>Katso <a href="http://wiki.mahara.org/System_Administrator\'s_Guide/Installing_Mahara">asennusoppaasta</a> ohjeet sen käynnistämiseksi.';

$string['csverroremptyfile'] = 'Csv-tiedosto on tyhjä';

$string['csvfile'] = 'CSV-tiedosto';

$string['csvfiledescription'] = 'Tiedosto käyttäjien perustamiseksi';

$string['currentadmins'] = 'Nykyiset pääkäyttäjät';

$string['currentmembers'] = 'Nykyiset jäsenet';

$string['currentstaff'] = 'Nykyiset ylläpitäjät';

$string['databasesize'] = 'Tietokannan koko';

$string['datathatwillbesent'] = 'Lähtevä data';

$string['dbcollationmismatch'] = 'A column of your database is using a collation that is not the same as the database default.  Please ensure all columns use the same collation as the database.' /** MISSING **/ ;

$string['dbnotutf8warning'] = 'You are not using a UTF-8 database. Mahara stores all data as UTF-8 internally. You may still attempt this upgrade but it is recommended that you convert your database to UTF-8.' /** MISSING **/ ;

$string['declinerequests'] = 'Hylkää pyynnöt';

$string['defaultaccountinactiveexpire'] = 'Käyttäjätilin oletuspassiivisuusaika';

$string['defaultaccountinactiveexpiredescription'] = 'Kuinka pitkään käyttäjätili pysyy auki, kun käyttäjä ei kirjaudu järjestelmään';

$string['defaultaccountinactivewarn'] = 'Varoitusaika käyttäjätilin passiivisuudelle/voimassaolon päättymiselle';

$string['defaultaccountinactivewarndescription'] = 'Aika, jolloin varoitusviesti lähetetään käyttäjille ennen kuin käyttäjätilit suljetaan tai ne tulevat passiiviksi';

$string['defaultaccountlifetime'] = 'Käyttäjätilin oletuskestoaika';

$string['defaultaccountlifetimedescription'] = 'Mikäli asetettu, käyttäjätilit suljetaan tämän ajan kuluttua siitä kun ne on luotu';

$string['defaultmembershipperiod'] = 'Oletusjäsenyysjakso';

$string['defaultmembershipperioddescription'] = 'Kuinka kauan uusi jäsen pysyy instituution jäsenenä';

$string['deletefailed'] = 'Poisto epäonnistui';

$string['deleteinstitution'] = 'Poista instituutio';

$string['deleteinstitutionconfirm'] = 'Haluatko varmasti poistaa tämän instituution?';

$string['deleteuser'] = 'Poista käyttäjä';

$string['deleteusernote'] = 'Huomaa, että tätä toimintoa <strong>ei voi perua</strong>.';

$string['deleteusers'] = 'Poista käyttäjät';

$string['deletingmenuitem'] = 'Poistetaan';

$string['disabledlockedfieldhelp'] = 'Huomio: Käytöstä poistetut valinnat koskevat profiilikenttiä, jotka on lukittu instituution "%s" asetuksissa. Nämä on lukittu palvelun tasolla, eikä niitä voi vaihtaa tässä.';

$string['disableexternalresources'] = 'Poista käytöstä ulkoiset resurssit käyttäjien html-koodissa';

$string['disableexternalresourcesdescription'] = 'Estää ulkoiset resurssit, esim. muilla palvelimilla olevat kuvat';

$string['discardpageedits'] = 'Hylkäätkö muutokset, jotka olet tehnyt tälle sivulle?';

$string['diskusage'] = 'Levyn käyttö';

$string['editadmins'] = 'Muokkaa pääkäyttäjiä';

$string['editlinksandresources'] = 'Muokkaa linkkejä ja resursseja';

$string['editmembers'] = 'Muokkaa jäseniä';

$string['editsitepages'] = 'Muokkaa palvelun sivuja';

$string['editsitepagesdescription'] = 'Muokkaa palvelun sivujen sisältöjä';

$string['editsitepagespagedescription'] = 'Tässä voit muokata sivuston sivujen sisältöjä kuten aloitussivua (sisään- ja uloskirjautuneita käyttäjiä erikseen) ja sivuja, jotka ovat linkitetty alatunnisteeseen.';

$string['editstaff'] = 'Muokkaa ylläpitäjiä';

$string['emailnoreplyaddress'] = 'System Mail Address';

$string['emailnoreplyaddressdescription'] = 'Emails come out as from this address';

$string['emailsettings'] = 'Email settings';

$string['emailsmtphosts'] = 'SMTP Host';

$string['emailsmtphostsdescription'] = 'SMTP server to be used for mail sending, e.g. <em>smtp1.example.com</em>';

$string['emailsmtppass'] = 'Pass';

$string['emailsmtpport'] = 'SMTP Port';

$string['emailsmtpportdescription'] = 'Specify port number if SMTP server uses port different from 25';

$string['emailsmtpsecure'] = 'SMTP Encryption';

$string['emailsmtpsecuredescription'] = 'If the SMTP server supports encryption, enable it here.';

$string['emailsmtpsecuressl'] = 'SSL';

$string['emailsmtpsecuretls'] = 'TLS';

$string['emailsmtpuser'] = 'User';

$string['emailsmtpuserdescription'] = 'If SMTP server requires authentication, enter user credentials in the corresponding fields';

$string['emailusersaboutnewaccount'] = 'Lähetä sähköpostiviesti käyttäjille käyttäjätileihin liittyvistä asioista';

$string['emailusersaboutnewaccountdescription'] = 'Lähetetäänkö käyttäjille sähköpostitiedotus heidän uuden käyttäjätilin ominaisuuksista';

$string['embeddedcontent'] = 'Upotettu sisältö';

$string['embeddedcontentdescription'] = 'Jos haluat, että käyttäjät voivat käyttää upotettuja videoita tai muuta ulkoista sisältöä, voit määritellä luotetut palvelut alla.';

$string['enablegroupcategories'] = 'Ota ryhmien kategoriat käyttöön';

$string['enablenetworking'] = 'Verkkoasetukset käytössä';

$string['enablenetworkingdescription'] = 'Mahara-palvelimesi saa keskustella muiden palveluita tarjoavien palvelimien kanssa';

$string['ensurepluginsexist'] = 'Tarkista, että kaikki asennetut pluginit löytyvät kansiosta %s, ja että niihin on lukuoikeus.';

$string['errors'] = 'Virheet';

$string['errorwhilesuspending'] = 'Virhe tapahtui lukitusyrityksessä';

$string['errorwhileunsuspending'] = 'Virhe tapahtui lukituksen poistamisessa';

$string['exportingnotsupportedyet'] = 'Käyttäjien profiilien vienti ei ole vielä tuettu';

$string['exportingusername'] = 'Viedään \'%s\'...';

$string['exportuserprofiles'] = 'Vie käyttäjien profiilit';

$string['externallink'] = 'Ulkoinen linkki';

$string['failedtoobtainuploadedleapfile'] = 'Järjestelmään ladattua Leap2A-tiedostoa ei voitu käsitellä';

$string['failedtounzipleap2afile'] = 'Leap2A-tiedoston purku epäonnistui. Tutki error log -tiedostoa saadaksesi lisätietoa';

$string['fileisnotaziporxmlfile'] = 'Tätä tiedostoa ei ole tunnistettu Zip- tai XML-tiedostoksi';

$string['filequota'] = 'Tiedostokoko (MB)';

$string['filequotadescription'] = 'Kokonaistila käytössä käyttäjän tiedostoalueella.';

$string['filtersinstalled'] = 'Suodattimet asennettu';

$string['footermenu'] = 'Sivun alavalikko';

$string['footermenudescription'] = 'Hallitse linkkejä sivun alavalikossa.';

$string['footerupdated'] = 'Alavalikko päivitetty';

$string['forcepasswordchange'] = 'Pakota salasananvaihto seuraavan kirjoittautumisen yhteydessä';

$string['forcepasswordchangedescription'] = 'Käyttäjä ohjataan salasananvaihtosivulle seuraavan kirjoittautumisen yhteydessä.';

$string['forceuserstochangepassword'] = 'Pakotettu salasanan vaihto?';

$string['forceuserstochangepassworddescription'] = 'Pakotetaanko käyttäjät vaihtamaan salasanansa esimmäisen kirjoittautumisen yhteydessä';

$string['fromversion'] = 'versiosta';

$string['generalsettingslegend'] = 'Yleiset asetukset';

$string['groupadmins'] = 'Pääkäyttäjät';

$string['groupadminsforgroup'] = "Pääkäyttäjät ryhmälle '%s'";

$string['groupadminsupdated'] = 'Ryhmien pääkäyttäjät päivitetty';

$string['groupcategories'] = 'Ryhmien kategoriat';

$string['groupcategoriesdescription'] = 'Lisää ja muokkaa ryhmäkategorioita';

$string['groupcategoriespagedescription'] = 'Ryhmiä luotaessa ne voi liittää johonkin näistä kategorioista. Kategorioita voi käyttää ryhmähaussa.';

$string['groupcategorydeleted'] = 'Kategoria poistettu';

$string['groupcountsbyjointype'] = 'Ryhmiä pääsyn mukaan';

$string['groupcountsbytype'] = 'Ryhmiä tyypin mukaan';

$string['groupdelete'] = 'Poista';

$string['groupmanage'] = 'Muokkaa';

$string['groupmemberaverage'] = 'Keskimäärin jokainen käyttäjä kuuluu %s:n ryhmään';

$string['groupmembers'] = 'Jäsenet';

$string['groupname'] = 'Ryhmän nimi';

$string['groupoptionsset'] = 'Ryhmien asetukset päivitetty.';

$string['groups'] = 'Ryhmät';

$string['groupsettingslegend'] = 'Ryhmien asetukset';

$string['groupstatstabletitle'] = 'Suurimmat ryhmät';

$string['grouptype'] = 'Ryhmän tyyppi';

$string['groupvisible'] = 'Näkyvyys';

$string['home'] = 'Aloitussivu (Työpöytä)';

$string['homepageinfo'] = 'Näytä etusivulla tietoja Maharasta';

$string['homepageinfodescription'] = 'Jos valittu, etusivulla näytetään tietoja Maharasta ja sen käytöstä. Kirjautuneet käyttäjät voivat piilottaa tiedot.';

$string['howdoyouwanttocreatethisuser'] = 'Miten haluat luoda tämän käyttäjän?';

$string['htmlfilters'] = 'HTML suodattimet';

$string['htmlfiltersdescription'] = 'Lisää uusia HTML Purifier -suodattimia';

$string['importednuserssuccessfully'] = 'Tuotu %d/%d käyttäjää';

$string['importfailedfornusers'] = '%d käyttäjää ei voitu tuoda (yhteensä %d)';

$string['importfile'] = 'Tuontitiedosto';

$string['importfileisnotazipfile'] = 'Tuontitiedostoa %s ei tunnistettu zip-tiedostoksi';

$string['importfilemissinglisting'] = 'Tuontitiedostosta puuttuu usernames.csv-tiedosto. Käytitkö Maharan vientitoimintoa sen luomiseen?';

$string['importfilenotafile'] = 'Virhe: tuntematon tiedostotyyppi';

$string['importfilenotreadable'] = 'Leap2A-tiedostoa %s ei voi lukea';

$string['information'] = 'Informaatio';

$string['install'] = 'Asenna';

$string['installation'] = 'Asennus';

$string['installed'] = 'Asennettu';

$string['installedpluginsmissing'] = 'Seuraavat pluginit on asennettu, mutta niitä ei enää löydy';

$string['installmahara'] = 'Asenna Mahara';

$string['installsuccess'] = 'Onnistuneesti asennettu versio';

$string['institution'] = 'Instituutio';

$string['institutionaddedsuccessfully2'] = 'Instituutio lisätty onnistuneesti';

$string['institutionadmin'] = 'Instituution pääkäyttäjä';

$string['institutionadmindescription'] = 'Mikäli asetettu, käyttäjä voi hallinnoida kaikkia instituution käyttäjiä';

$string['institutionadministration'] = 'Instituutiohallinta';

$string['institutionadministrator'] = 'Instituution pääkäyttäjä';

$string['institutionadmins'] = 'Instituution pääkäyttäjät';

$string['institutionadminsdescription'] = 'Anna instituution pääkäyttäjän oikeudet';

$string['institutionadminuserspagedescription'] = 'Tässä voit valita ketkä ovat instituution pääkäyttäjiä. Nykyiset pääkäyttäjät on listattu oikealla, ja mahdolliset pääkäyttäjät ovat vasemmalla.';

$string['institutionauth'] = 'Instituution valtuuttajat';

$string['institutionautosuspend'] = 'Lukitse automaattisesti vanhentuneet instituutiot';

$string['institutionautosuspenddescription'] = 'Mikäli asetettu, instituutiot, joiden voimassaoloaika on päättynyt, lakkautetaan automaattisesti';

$string['institutiondeletedsuccessfully'] = 'Instituutio poistettu onnistuneesti';

$string['institutiondetails'] = 'Instituution yksityiskohdat';

$string['institutiondisplayname'] = 'Instituution nimi käyttäjiä varten';

$string['institutionexpiry'] = 'Instituution erääntymispäivämäärä';

$string['institutionexpirydescription'] = 'Päivämäärä, jolloin %s instituution jäsenyys lukitaan.';

$string['institutionexpirynotification'] = 'Varoaika ennen instituution voimassaolon erääntymistä';

$string['institutionexpirynotificationdescription'] = 'Ilmoitus lähetetään palvelun ja instituutioiden pääkäyttäjille ennen palvelun erääntymistä';

$string['institutionfiles'] = 'Instituution tiedostot';

$string['institutionfilesdescription'] = 'Tuo ja hallinnoi instituution sivujen tiedostoja';

$string['institutionmaxusersexceeded'] = 'Instituutio on täynnä, kasvata instituution käyttäjien kokonaismäärää ennen kuin lisäät tämän käyttäjän.';

$string['institutionmembers'] = 'Instituution jäsenet';

$string['institutionmembersdescription'] = 'Liitä käyttäjiä instituutioihin';

$string['institutionmemberspagedescription'] = 'Tällä sivulla näet käyttäjät, jotka ovat pyytäneet instituutiosi jäsenyyttä ja voit lisätä heidät jäseniksi. Voit myös poistaa instituutiosi käyttäjiä ja kutsua käyttäjiä liittymään instituutioosi.';

$string['institutionname'] = 'Instituution nimi';

$string['institutionnamealreadytaken'] = 'Instituution nimi on jo käytössä';

$string['institutions'] = 'Instituutiot';

$string['institutionsdescription'] = 'Aseta ja hallinnoi asennettuja instituutioita';

$string['institutionsettings'] = 'Instituution asetukset';

$string['institutionsettingsdescription'] = 'Tästä voit vaihtaa käyttäjän instituutioasetuksia.';

$string['institutionsettingslegend'] = 'Instituutioiden asetukset';

$string['institutionstaff'] = 'Instituution ylläpitäjät';

$string['institutionstaffdescription'] = 'Anna ylläpitäjän oikeudet';

$string['institutionstaffuserspagedescription'] = 'Tässä voit valita instituutiosi ylläpitäjät. Nykyiset ylläpitäjät ovat oikealla ja mahdolliset ylläpitäjät vasemmalla.';

$string['institutionstudentiddescription'] = 'Valinnainen tunniste instituutiolle. Ei käyttäjän muokattavissa.';

$string['institutionsuspended'] = 'Instituution toiminta keskeytetty';

$string['institutionunsuspended'] = 'Instituution toiminnan keskeytys lopetettu';

$string['institutionupdatedsuccessfully'] = 'Instituution tiedot päivitetty onnistuneesti.';

$string['institutionuserserrortoomanyinvites'] = 'Kutsujasi ei ole lähetetty. Olemassa olevien jäsenten ja keskeneräisten kutsujen määrä ei voi ylittää instituution maksimikäyttäjämäärää. Voit kutsua vähemmän käyttäjiä, poistaa joitakin käyttäjiä instituutiosta tai pyytää palveluntarjoajaa nostamaan maksimikäyttäjämäärää.';

$string['institutionuserserrortoomanyusers'] = 'Käyttäjiä ei ole lisätty. Jäsenien määrä ei voi ylittää instituution maksimikäyttäjämäärää. Voit lisätä vähemmän käyttäjiä, poistaa joitakin käyttäjiä instituutiosta tai pyytää palveluntarjoajaa nostamaan maksimikäyttäjämäärää.';

$string['institutionusersinstructionsmembers'] = 'Käyttäjälistaus vasemmalla näyttää kaikki instituution jäsenet. Voit käyttää etsi-laatikkoa vähentääksesi näytettävien käyttäjien määrää. Siirtääksesi käyttäjiä instituutiosta, siirrä joitakin käyttäjiä oikealle puolelle valitsemalla yksi tai useampi käyttäjä vasemmalta puolelta ja klikkaamalla sen jälkeen nuolta oikealla. Käyttäjät, jotka valitsit, siirtyvät oikealle. Siirrä käyttäjiä -painike siirtää kaikki oikealla puolella olevat käyttäjät instituutiosta. Vasemmalla puolella olevat käyttäjät pysyvät instituutiossa.';

$string['institutionusersinstructionsnonmembers'] = 'Käyttäjälistaus vasemmalla näyttää kaikki ne käyttäjät, jotka eivät vielä ole instituution jäseniä. Voit käyttää etsi-laatikkoa vähentääksesi näytettävien käyttäjien määrää. Kutsuaksesi käyttäjiä instituutioon, siirrä ensin joitakin käyttäjiä oikealle puolelle valitsemalla yksi tai useampi käyttäjä ja klikkaamalla sen jälkeen oikealla puolella olevaa nuolta siirääksesi kyseiset käyttäjät oikeanpuoleiseen listaan. Kutsu käyttäjiä -painike lähettää kutsun kaikille oikealla puolella oleville käyttäjille. Nämä käyttäjät eivät liity instituutioon ennen kuin he ovat hyväksyneet kutsun.';

$string['institutionusersinstructionsrequesters'] = 'Käyttäjälistaus vasemmalla näyttää kaikki ne käyttäjät, jotka ovat pyytäneet instituution jäsenyyttä. Voit käyttää etsi-laatikkoa vähentääksesi näytettävien käyttäjien määrää. Jos haluat lisätä käyttäjiä instituutioon tai hylätä jäsenyyspyyntöjä, siirrä ensin joitakin käyttäjiä oikealle puolelle valitsemalla yksi tai useampi käyttäjä ja sen jälkeen klikkaamalla oikealla puolella olevaa nuolta. Lisää jäseniä -painike lisää kaikki käyttäjät oikealta puolelta instituutioon. Hylkää pyynnöt -painike poistaa kaikkien oikealla puolella olevien käyttäjien jäsenyyspyynnöt.';

$string['institutionusersmembers'] = 'Henkilöt, jotka ovat jo instituution jäseniä';

$string['institutionusersnonmembers'] = 'Henkilöt, jotka eivät ole vielä pyytäneet jäsenyyttä';

$string['institutionusersrequesters'] = 'Henkilöt, jotka ovat pyytäneet instituution jäsenyyttä';

$string['institutionusersupdated_addUserAsMember'] = 'Käyttäjät lisätty';

$string['institutionusersupdated_declineRequestFromUser'] = 'Pyyntö hylätty';

$string['institutionusersupdated_inviteUser'] = 'Kutsut lähetetty';

$string['institutionusersupdated_removeMembers'] = 'Käyttäjät poistettu';

$string['institutionviews'] = 'Instituution sivut';

$string['institutionviewsdescription'] = 'Luo ja hallinnoi instituution sivuja ja sivupohjia';

$string['invalidfilename'] = 'Tiedostoa "%s" ei ole';

$string['invalidlistingfile'] = 'Virheellinen käyttäjälistaus. Käytitkö Maharan vientitoimintoa sen luomiseen?';

$string['invitationsent'] = 'Kutsu lähetetty';

$string['invitedby'] = 'Kutsuja';

$string['inviteusers'] = 'Kutsu Käyttäjät';

$string['inviteuserstojoin'] = 'Pyydä käyttäjät liittymään instituutioon';

$string['jsrequiredforupgrade'] = 'Sinun tulee sallia javascript suorittaaksesi asenntuksen tai päivityksen.';

$string['language'] = 'Kieli';

$string['latestversionis'] = 'viimeisin versio on <a href="%s">%s</a>';

$string['leap2aimportfailed'] = '<p><strong>Leap2a-tiedoston tuonti epäonnistui.</strong></p><p>Tämä voi johtua siitä, että valitsemasi tiedosto ei ole oikean muotoinen tai sen versiota ei tueta tässä Maharassa. On myös mahdollista, että järjestelmässä on virhe.</p><p><a href="">Palaa takaisin ja yritä uudelleen</a>. Jos virhe toistuu, voit kysyä <a href="http://mahara.org/forums/">Maharan foorumista</a> apua. Ratkaisua varten sinun tulee mahdollisesti antaa kopio tiedostostasi.</p>';

$string['linkedto'] = 'Linkitetty kohteeseen';

$string['linksandresourcesmenu'] = 'Linkit ja resurssit -valikko';

$string['linksandresourcesmenupagedescription'] = 'Linkit ja resurssit -valikko tulee näkyviin kaikille käyttäjille suurimmalla osalla sivuista. Voit lisätä linkkejä toisille internet-sivuille ja tallennettuihin %spalvelun tiedostoihin%s.';

$string['loadingmenuitems'] = 'Lataa tiedostoja';

$string['loadmenuitemsfailed'] = 'Tiedostojen lataaminen epäonnistui';

$string['loadsitepagefailed'] = 'Sivun lataaminen epäonnistui';

$string['localdatasuccess'] = 'Paikallisten räätälöintien asennus onnistui';

$string['loggedinmenu'] = 'Kirjautuneen käyttäjän linkit ja resurssit';

$string['loggedinsince'] = '%s tänään, %$2s jälkeen %$1s, %s yhteensä';

$string['loggedouthome'] = 'Kirjautumattoman käyttäjän etusivu';

$string['loggedoutmenu'] = 'Julkiset linkit ja resurssit';

$string['loginas'] = 'Kirjaudu käyttäjänä';

$string['loginasdenied'] = 'Sinulla ei ole oikeutta kirjautua toisena käyttäjänä';

$string['loginasoverridepasswordchange'] = 'Koska esiinnyt toisena käyttäjänä, voit %sohittaa salasanan vaihdon%s.';

$string['loginasrestorenodata'] = 'Ei palautettavaa käyttäjätietoa';

$string['loginastwice'] = 'Yritys kirjautua toisena käyttäjä kun on jo kirjauduttu toisena käyttäjänä';

$string['loginasuser'] = 'Kirjaudu käyttäjänä %s';

$string['maharaversion'] = 'Maharan versio';

$string['managegroupdescription'] = 'Käytä lomaketta ryhmän pääkäyttäjien lisäämiseen ja poistamiseen. Jos poistat pääkäyttäjän, tämä pysyy silti ryhmän jäsenenä.';

$string['managegroups'] = 'Hallitse ryhmiä';

$string['manageinstitutions'] = 'Instituutiohallinta';

$string['maxuseraccounts'] = 'Maksimimäärä käyttäjätilejä sallittu';

$string['maxuseraccountsdescription'] = 'Maksimimäärä käyttäjätilejä, jotka voi olla liitettynä instituutioon. Mikäli rajaa ei ole, tämä kenttä tulee jättää tyhjäksi.';

$string['membershipexpiry'] = 'Jäsenyys erääntyy';

$string['membershipexpirydescription'] = 'Päivämäärä, jolloin käyttäjä automaattisesti siirretään pois instituutiosta.';

$string['menuitemdeleted'] = 'poistettu';

$string['menuitemsaved'] = 'tallennettu';

$string['menuitemsloaded'] = 'ladattu';

$string['menus'] = 'Valikot';

$string['menusdescription'] = 'Hallitse linkkejä ja tiedostoja Linkit ja resurssit -sekä Alaviite-valikoissa';

$string['missingplugin'] = 'Asennettua pluginia %s ei löydy';

$string['name'] = 'Nimi';

$string['networking'] = 'Verkkoasetukset';

$string['networkingdescription'] = 'Konfiguroi verkkoasetukset Maharaa varten';

$string['networkingdisabled'] = 'Verkkoasetukset kytketty pois päältä';

$string['networkingenabled'] = 'Verkkoasetukset kytketty päälle';

$string['networkingextensionsmissing'] = 'Valitettavasti et voi konfiguroida Maharan verkkoasetuksia koska PHP-asennuksesta puuttuu yksi tai useampia vaadittuja laajennuksia:';

$string['networkingpagedescription'] = 'Maharan verkko-ominaisuus mahdollistaa kommunikoinnin muiden palveluiden kanssa, kuten Moodlen tai toisen Maharan. Jos tämä on käytössä, käyttäjät voivat kirjautua Single sign on -toiminnon avulla.';

$string['networkingunchanged'] = 'Verkkoasetuksia ei ole muutettu';

$string['newfiltersdescription'] = 'Jos olet ladannut uusia HTML-suodattimia, asenna ne purkamalla kansioon %s ja painamalla alla olevaa nappia.';

$string['newusercreated'] = 'Uusi käyttäjätili luotu onnistuneesti';

$string['newuseremailnotsent'] = 'Tervetuloa -sähköpostin lähettäminen epäonnistui.';

$string['noauthpluginforinstitution'] = 'Palveluntarjoajasi ei ole konfiguroinnut tunnistautumispluginia tälle instituutiolle.';

$string['nofiltersinstalled'] = 'HTML-suodattamia ei ole asennettu.';

$string['noinstitutions'] = 'Ei instituutioita';

$string['noinstitutionsdescription'] = 'Luo ensin instituutio, johon liität käyttäjiä.';

$string['noleap2axmlfiledetected'] = 'Leap2a.xml-tiedostoa ei löydy - ole hyvä ja tarkista, että tuontitiedostosi on oikea';

$string['none'] = 'Ei mitään';

$string['nositefiles'] = 'Ei tallennettuja palvelun tiedostoja';

$string['notadminforinstitution'] = 'Et ole tämän instituution pääkäyttäjä';

$string['nothingtoupgrade'] = 'Ei päivitettävää';

$string['notificationssaved'] = 'Ilmoitusasetukset tallennettu';

$string['notinstalled'] = 'Ei asennettu';

$string['noupgrades'] = 'Ei ole päivitettävää. Olet ajantasalla!';

$string['nousersselected'] = 'Ei käyttäjiä valittuina';

$string['nousersupdated'] = 'Käyttäjiä ei ole päivitetty';

$string['onlyshowingfirst'] = 'Näytetään vain ensimmäiset';

$string['pagename'] = 'Sivun nimi';

$string['pagesaved'] = 'Sivu tallennettu';

$string['pagetext'] = 'Sivun teksti';

$string['passwordchangenotallowed'] = 'The chosen authentication method does not allow changes to the password.';

$string['pathtoclam'] = 'Polku clam-sovellukseen';

$string['pathtoclamdescription'] = 'Tiedostojärjestelmän polku clamscan tai clamdscan -sovellukseen';

$string['performinginstallation'] = 'Suorittaa asennusta...';

$string['performingupgrades'] = 'Päivittää...';

$string['pluginadmin'] = 'Lisäosien hallinnointi';

$string['pluginadmindescription'] = 'Asenna ja määritä lisäosia';

$string['potentialadmins'] = 'Mahdolliset pääkäyttäjät';

$string['potentialstaff'] = 'Mahdolliset ylläpitäjät';

$string['privacy'] = 'Yksityisyyden suoja';

$string['promiscuousmode'] = 'Rekisteröi automaattisesti kaikki hostit';

$string['promiscuousmodedescription'] = 'Luo instituutio jokaiselle hostille, joka on kytketty palveluun ja salli niiden käyttäjien kirjoittautua Maharaan';

$string['promiscuousmodedisabled'] = 'Automaattinen rekisteröinti pois päältä';

$string['promiscuousmodeenabled'] = 'Automaattinen rekisteröinti päällä';

$string['proxyaddress'] = 'Välityspalvelimen osoite';

$string['proxyaddressdescription'] = 'Jos palvelusi on yhteydessä internetiin välityspalvelimen kautta, määrittele välityspalvelimet  muodossa <em>hostname:portnumber</em>';

$string['proxyaddressset'] = 'Välityspalvelimen osoite asetettu';

$string['proxyauthcredentials'] = 'Välityspalvelimen tunnus ja salasana';

$string['proxyauthcredentialsdescription'] = 'Syötä välityspalvelimesi tunnus ja salasana  muodossa <em>username:password</em>';

$string['proxyauthcredntialsset'] = 'Välityspalvelimen tunnus ja salasana asetettu';

$string['proxyauthmodel'] = 'Välityspalvelimen tunnistautumistapa';

$string['proxyauthmodelbasic'] = 'Basic (NCSA)';

$string['proxyauthmodeldescription'] = 'Valitse välityspalvelimellesi tarvittava tunnistautumistapa';

$string['proxyauthmodelset'] = 'Välityspalvelimen tunnistautumistapa asetettu';

$string['proxysettings'] = 'Välityspalvelimen asetukset';

$string['public'] = 'julkinen';

$string['publickey'] = 'Julkinen avain';

$string['publickeydescription2'] = 'Tämä julkinen avain generoidaan automaattisesti ja vaihdetaan joka %s. päivä';

$string['publickeyexpires'] = 'Julkinen avain erääntyy';

$string['registerterms'] = 'Käyttöehtojen hyväksyminen';

$string['registertermsdescription'] = "Käyttäjien tulee hyväksyä palvelun käyttöehdot. Muokkaa käyttöehdot haluamaksesi ennen tämän käyttöönottoa.";

$string['registerthismaharasite'] = 'Rekisteröi tämä Mahara-asennus';

$string['registeryourmaharasite'] = 'Rekisteröi Mahara-asennuksesi';

$string['registeryourmaharasitedetail'] = '<p>Voit rekisteröidä Mahara-palvelusi osoitteessa <a href="http://mahara.org/">mahara.org</a>. Rekisteröinti on ilmaista ja se auttaa meitä rakentamaan kuvaa Maharan käytöstä ympäri maailmaa.</p> <p>Näet tiedot jotka lähetetään mahara.org –osoitteeseen, mitään tietoa, joka voidaan yksilöidä sinuun, ei lähetetä.</p> <p>Jos valitset &quot;send weekly updates&quot;, Mahara lähettää automaattisesti päivityksen mahara.org –osoitteeseen kerran viikossa päivitetyistä tiedoistasi.</p> <p>Rekisteröinti poistaa tämän ilmoituksen. Voit vaihtaa lähetetäänkö viikottaiset päivitykset <a href="%sadmin/site/options.php">site options</a> -sivulla.</p>';

$string['registeryourmaharasitesummary'] = '
<p>Voit rekisteröidä Mahara-palvelusi osoitteessa <a href="http://mahara.org/">mahara.org</a>, jotta saisimme paremman käsityksen Maharan käytöstä maailmalla. Rekisteröiminen poistaa tämän ilmoituksen.</p> 
<p>Rekisteröinnin voit tehdä <strong><a href="%sadmin/registersite.php">rekisteröintisivulla.</a></strong> Samalla voit tarkastella tietoja, jotka lähetetään.</p>';

$string['registrationallowed'] = 'Rekisteröityminen sallittu?';

$string['registrationalloweddescription2'] = 'Valitse voivatko käyttäjät rekisteröityä tähän instituutioon rekisteröitymislomakkeella. Jos rekisteröinti on pois käytöstä, käyttäjät eivät voi liittyä tai poistua tästä insituutiosta eivätkä nämä voi poistaa tunnuksiaan.';

$string['registrationfailedtrylater'] = 'Rekisteröityminen epäonnistui ja palautti virhekoodin %s. Ole hyvä ja yritä myöhemmin uudelleen.';

$string['registrationsuccessfulthanksforregistering'] = 'Rekisteröinti onnistui - kiitos rekisteröitymisestäsi!';

$string['reinstall'] = 'Asenna uudelleen';

$string['release'] = 'versio %s (%s)';

$string['remoteavatars'] = 'Näytä Gravatar-käyttäjäkuvat';

$string['remoteavatarsdescription'] = 'Jos valittu, <a href="http://www.gravatar.com">Gravatar-palvelua</a> käytetään oletuksena profiilikuville.';

$string['remoteusername'] = 'Käyttäjätunnus ulkoista tunnistautumista varten';

$string['remoteusernamedescription'] = 'Jos käyttäjä kirjoittautuu %s palveluun toisesta palvelusta XMLRPC -tunnistautumispluginin kautta, tämä on käyttäjätunnus, jolla tunnistetaan käyttäjä toisessa järjestelmässä';

$string['removeuserfrominstitution'] = 'Poista käyttäjä tästä instituutiosta';

$string['removeusers'] = 'Poista käyttäjät';

$string['removeusersfrominstitution'] = 'Poista käyttäjät instituutiosta';

$string['reopensite'] = 'Avaa palvelu uudestaan';

$string['reopensitedetail'] = 'Palvelu on suljettu. Ainoastaan pääkäyttäjät voivat olla kirjautuneina, kunnes päivitys on valmis.';

$string['requestto'] = 'Liittymispyyntö:';

$string['resetpassword'] = 'Aseta salasana uudelleen';

$string['resetpassworddescription'] = 'Tähän kenttään syötetty teksti tulee korvaamaan käyttäjän nykyisen salasanan';

$string['resultsof'] = 'tulosta, kaikkiaan';

$string['runningnormally'] = 'toimii normaalisti';

$string['runupgrade'] = 'Käynnistä päivitys';

$string['savechanges'] = 'Tallenna muutokset';

$string['savefailed'] = 'Tallennus epäonnistui';

$string['savingmenuitem'] = 'Tallennetaan';

$string['searchplugin'] = 'Hakukone';

$string['searchplugindescription'] = 'Valittu plugin hakutoimintoa varten';

$string['searchsettingslegend'] = 'Hakuasetukset';

$string['searchusernames'] = 'Hae käyttäjätunnuksilla';

$string['searchusernamesdescription'] = 'Käyttäjätunnuksia voi käyttää käyttäjähaussa.';

$string['securitysettingslegend'] = 'Tietoturva-asetukset';

$string['sendweeklyupdates'] = 'Lähetä viikottaiset päivitykset?';

$string['sendweeklyupdatesdescription'] = 'Jos asetus on päällä, palvelusi lähettää viikottaiset päivitykset osoitteeseen mahara.org joidenkin palveluasi koskevien tilastojen kanssa';

$string['sessionlifetime'] = 'Istunnon kestoaika';

$string['sessionlifetimedescription'] = 'Aika minuutteina, jonka jälkeen passiivinen kirjautunut käyttäjä poistetaan automaattisesti järjestelmästä';

$string['setsiteoptionsfailed'] = 'Vaihtoehdon %s asettaminen epäonnistui';

$string['settingsfor'] = 'Asetukset koskien:';

$string['showonlineuserssideblock'] = 'Näytä kirjautuneet käyttäjät';

$string['showonlineuserssideblockdescription'] = 'Jos valittu, oikessa palstassa näkyy listaus tällä hetkellä kirjautuneista käyttäjistä.';

$string['showselfsearchsideblock'] = 'Hakutoiminto portfolioihin käytössä';

$string['showselfsearchsideblockdescription'] = 'Näytä "Hae omasta portfoliosta" -kenttä omassa portfoliossa';

$string['showtagssideblock'] = 'Avainsanapilvi käytössä';

$string['showtagssideblockdescription'] = 'Jos sallittu, käyttäjät näkevät Portfolio-osiossa alueen, johon on listattuna heidän useimmiten käyttämänsä avainsanat';

$string['simple'] = 'Suppea';

$string['siteaccountsettings'] = 'Tilin asetukset';

$string['siteadmin'] = 'Palvelun pääkäyttäjä';

$string['siteadmins'] = 'Palvelun pääkäyttäjät';

$string['sitecountrydescription'] = 'Käyttäjän oletusmaa';

$string['sitedefault'] = 'Palvelun oletusasetus';

$string['sitefile'] = 'Palvelun tiedosto';

$string['sitefiles'] = 'Palvelun tiedostot';

$string['sitefilesdescription'] = 'Tuo ja hallinnoi tiedostoja, jotka voidaan sijoittaa Linkit ja resurssit- ja Palvelun sivut -valikkoihin';

$string['siteinformation'] = 'Palvelun tiedot';

$string['siteinstalled'] = 'Asennettu';

$string['sitelanguagedescription'] = 'Palvelun oletuskieli';

$string['sitename'] = 'Palvelun nimi';

$string['sitenamedescription'] = 'Palvelun nimi tulee näkyvin eri kohdissa palvelun sisällä ja palvelun lähettämissä viesteissä';

$string['siteoptions'] = 'Palvelun asetukset';

$string['siteoptionsdescription'] = 'Määritä perusasetukset kuten nimi, kieli ja teema';

$string['siteoptionspagedescription'] = 'Tässä voit määritellä yleisiä asetuksia, jotka tulevat voimaan oletuksena kaikkialla palvelussa. <BR> Huomio: käytöstä poistetut valinnat on ohitettu config.php tiedostossa.';

$string['siteoptionsset'] = 'Palvelun asetukset päivitetty';

$string['sitepageloaded'] = 'Palvelun sivu ladattu';

$string['siteregistered'] = 'Palvelu on rekisteröity. Voit asettaa viikottaiset päivitykset päälle tai pois <a href="%sadmin/site/options.php">palvelun asetussivulla</a>.</p>';

$string['sitesettingslegend'] = 'Palvelun asetukset';

$string['sitestaff'] = 'Palvelun ylläpitäjä';

$string['sitestatistics'] = 'Palvelun tilastot';

$string['sitethemedescription'] = 'Palvelun oletusteema - jos teemaa ei näy listalla, syy voi löytyä virhelokista';

$string['siteviews'] = 'Palvelun sivut';

$string['siteviewsdescription'] = 'Luo ja hallinnoi sivuja ja sivupohjia koko palvelua varten';

$string['smallviewheaders'] = 'Minimaalinen otsikkopalkki portfoliosivuilla';

$string['smallviewheadersdescription'] = 'Jos valittu, portfoliosivuilla käytetään pienempää navigointipalkkia.';

$string['spamhaus'] = 'Ota käyttöön Spamhaus URL -roskapostisuodatinlista';

$string['spamhausdescription'] = 'Jos valittu, osoitteet tarkastetaan Spamhaus DNSBL -tietokannasta';

$string['staffusers'] = 'Palvelun ylläpitäjät';

$string['staffusersdescription'] = 'Anna ylläpitäjäoikeudet';

$string['staffuserspagedescription'] = 'Tässä voit valita ketkä käyttäjät ovat tämän palvelun ylläpitäjiä. Nykyiset ylläpitäjät ovat oikealla ja mahdolliset ylläpitäjät vasemmalla.';

$string['staffusersupdated'] = 'Ylläpitäjä käyttäjät päivitetty';

$string['statsmaxfriends'] = 'On %s kaveria (eniten käyttäjällä <a href="%s">%s</a>, %d)';

$string['statsmaxgroups'] = 'Kuuluu %s ryhmään (eniten käyttäjällä <a href="%s">%s</a>, %d)';

$string['statsmaxquotaused'] = 'Käyttää %s tiedostokiintiöstään (suurin kulutus käyttäjällä <a href="%s">%s</a>, %s)';

$string['statsmaxviews'] = 'On %s sivua (eniten käyttäjällä <a href="%s">%s</a>, %d)';

$string['statsnofriends'] = 'Ei ole kavereita :(';

$string['statsnogroups'] = 'Ei kuulu mihinkään ryhmään :(';

$string['statsnoviews'] = 'Ei ole sivuja :(';

$string['studentid'] = 'ID Numero';

$string['successfullyinstalled'] = 'Maharan asennus onnistunut!';

$string['surbl'] = 'Ota käyttöön SURBL URL -roskapostisuodatinlista';

$string['surbldescription'] = 'Jos valittu, osoitteet tarkastetaan SURBL DNSBL -tietokannasta';

$string['suspenddeleteuser'] = 'Lukitse/poista käyttäjä';

$string['suspenddeleteuserdescription'] = 'Tässä voit lukita tai kokonaan poistaa käyttäjätilin. Lukitut käyttäjät eivät pysty kirjautumaan ennen kuin lukitus on lopetettu. Huomaa, että lukitus voidaan peruuttaa, mutta poistoa <strong>ei voi</strong>.';

$string['suspended'] = 'Lukittu';

$string['suspendedby'] = '%s on lukinnut tämän käyttäjätilin';

$string['suspendedinstitution'] = 'LUKITTU';

$string['suspendedinstitutionmessage'] = 'Instituutio on lukittu';

$string['suspendedreason'] = 'Lukituksen syy';

$string['suspendedreasondescription'] = 'Teksti näytetään käyttäjälle seuraavan sisäänkirjautumisyrityksen yhteydessä.';

$string['suspendedusers'] = 'Lukitut käyttäjät';

$string['suspendedusersdescription'] = 'Lukitut tai passiiviset käyttäjät jotka käyttävät palvelua';

$string['suspendingadmin'] = 'Lukituksen hallinnointi';

$string['suspendinstitution'] = 'Instituution lukitus';

$string['suspendinstitutiondescription'] = 'Tässä voit lukita instituution. Lukittujen instituutioiden käyttäjät eivät pysty kirjautumaan sisään ennen kuin instituution lukitus on poistettu.';

$string['suspenduser'] = 'Käyttäjätilin lukitus';

$string['suspensionreason'] = 'Lukituksen syy';

$string['tagssideblockmaxtags'] = 'Maksimimäärä avainsanoja pilvessä';

$string['tagssideblockmaxtagsdescription'] = 'Oletusmäärä avainsanoja käyttäjän avainsanapilvessä';

$string['termsandconditions'] = 'Käyttöehdot';

$string['thefollowingupgradesareready'] = 'Seuraavat päivitykset ovat valmiina:';

$string['thisuserissuspended'] = 'Tämä käyttäjätili on lukittu';

$string['toversion'] = 'versioon';

$string['trustedsites'] = 'Luotetut palvelut';

$string['type'] = 'Tyyppi';

$string['unabletoreadbulkimportdir'] = 'Hakemistoa %s ei voi lukea';

$string['unabletoreadcsvfile'] = 'csv-tiedostoa %s ei voi lukea';

$string['unsuspendinstitution'] = 'Lukitsemattomat instituutiot';

$string['unsuspendinstitutiondescription'] = 'Tässä voit poistaa instituution lukituksen. Lukittujen instituutioiden käyttäjät eivät pysty kirjautumaan sisään ennen kuin instituution lukitus on avattu.<br/><strong>Varo:</strong>Instituution lukituksen avaaminen ilman, että sen erääntymispäivä asetetaan uudestaan tai poistetaan käytöstä, voi johtaa päivittäiseen uudelleen lukittumiseen.';

$string['unsuspendinstitutiondescription_top'] = '<em>Varo:</em> Instituution lukituksen avaaminen ilman, että sen erääntymispäivä asetetaan uudestaan tai poistetaan käytöstä, voi johtaa päivittäiseen uudelleen lukittumiseen.';

$string['unsuspendinstitutiondescription_top_instadmin'] = 'Lukittujen instituutioiden käyttäjät eivät voi kirjautua. Ota yhteyttä ylläpitoon instituution avaamiseksi.';

$string['unsuspenduser'] = 'Poista lukitus';

$string['unsuspendusers'] = 'Poista lukitukset';

$string['unzipfailed'] = 'Leap2A-tiedoston %s purku epäonnistui. Katso virhelokista lisätietoja.';

$string['unzipnotinstalled'] = 'Your system does not have the unzip command, or $cfg->pathtounzip is misconfigured. Please install unzip to enable importing a zipped export file or fix the $cfg->pathtounzip setting.';

$string['updatesiteoptions'] = 'Päivitä palvelun asetukset';

$string['upgradefailure'] = 'Päivitys epäonnistui!';

$string['upgradeloading'] = 'Lähettää...';

$string['upgrades'] = 'Päivitykset';

$string['upgradesuccess'] = 'Päivitys onnistui';

$string['upgradesuccesstoversion'] = 'Päivitys onnistui versioon';

$string['uploadcopyright'] = 'Lataa Tekijänoikeuslausunto';

$string['uploadcsv'] = 'Lisää käyttäjiä CSV-tiedoston avulla';

$string['uploadcsvdescription'] = 'Tuo uusia käyttäjiä sisältävä CSV-tiedosto';

$string['uploadcsverroremailaddresstaken'] = 'Tiedoston rivi %s sisältää sähköpostiosoitteen, joka on jo toisen käyttäjän käytössä';

$string['uploadcsverrorincorrectnumberoffields'] = 'Virhe rivillä %s: Tämä rivi ei sisällä oikeaa määrää kenttiä';

$string['uploadcsverrorinvalidemail'] = 'Virhe rivillä %s: Käyttäjän sähköpostiosoite ei ole oikeassa muodossa';

$string['uploadcsverrorinvalidfieldname'] = 'Kentän nimi "%s" ei kelpaa, tai rivissä on enemmän kenttiä kuin on määritelty ylätunnisteessa';

$string['uploadcsverrorinvalidpassword'] = 'Error on line %s of your file: Passwords must be at least six characters long and contain at least one digit and two letters';

$string['uploadcsverrorinvalidusername'] = 'Virhe tiedoston rivillä %s: Käyttäjätunnus ei ole oikeassa muodossa';

$string['uploadcsverrormandatoryfieldnotspecified'] = 'Tiedoston rivistä %s puuttuu kenttä "%s"';

$string['uploadcsverrornorecords'] = 'Tiedosto näyttää olevan tyhjä (vaikka ylätunniste on ok)';

$string['uploadcsverrorrequiredfieldnotspecified'] = 'Vaadittua kenttää "%s" ei ole määritelty formaattirivissä';

$string['uploadcsverrorunspecifiedproblem'] = 'CSV-tiedoston sisältöä ei ole jostakin syystä lisätty. Jos tiedoston formaatti on oikeassa muodossa, ongelma johtuu bugista ja sinun tulisi <a href="https://eduforge.org/tracker/?func=add&group_id=176&atid=739">tehdä bugiraportti</a>, johon tulisi liittää CSV-tiedosto (muista tyhjentää salasanat!) ja jos mahdollista, myös error log -tiedosto';

$string['uploadcsverroruseralreadyexists'] = 'Rivin %s käyttäjätunnus "%s" on jo olemassa';

$string['uploadcsvfailedusersexceedmaxallowed'] = 'Käyttäjiä ei ole lisätty koska tiedostossasi on liian monta käyttäjää. Käyttäjämäärä olisi ylittänyt instituution maksimikäyttäjämäärän.';

$string['uploadcsvinstitution'] = 'Uusien käyttäjien instituutio ja tunnistautumismenetelmä';

$string['uploadcsvpagedescription2'] = '<p>You may use this facility to upload new users via a <acronym title="Comma Separated Values">CSV</acronym> file.</p>
   
<p>The first row of your CSV file should specify the format of your CSV data. For example, it should look like this:</p>

<pre>username,password,email,firstname,lastname,studentid</pre>

<p>This row must include the <tt>username</tt>, <tt>password</tt>, <tt>email</tt>, <tt>firstname</tt> and <tt>lastname</tt> fields. It must also includes fields that you have made mandatory for all users to fill out, and any fields locked for the institution you are uploading the users for. You can <a href="%s">configure the mandatory fields</a> for all institutions, or <a href="%s">configure the locked fields for each institution</a>.</p>

<p>Your CSV file may include any other profile fields as you require. The full list of fields is:</p>

%s' /** MISSING **/ ;

$string['uploadcsvpagedescription2institutionaladmin'] = '<p>You may use this facility to upload new users via a <acronym title="Comma Separated Values">CSV</acronym> file.</p>

<p>The first row of your CSV file should specify the format of your CSV data. For example, it should look like this:</p>

<pre>username,password,email,firstname,lastname,studentid</pre>

<p>This row must include the <tt>username</tt>, <tt>password</tt>, <tt>email</tt>, <tt>firstname</tt> and <tt>lastname</tt> fields. It must also include any fields that the site administrator has made mandatory, and any fields that are locked for your institution. You can <a href="%s">configure the locked fields</a> for the institution(s) you manage.</p>

<p>Your CSV file may include any other profile fields as you require. The full list of fields is:</p>

%s' /** MISSING **/ ;

$string['uploadcsvsomeuserscouldnotbeemailed'] = 'Sähköpostia ei voitu lähettää kaikille käyttäjille. Joko näiden osoitteet ovat virheellisiä tai järjestelmän lähetysasetuksia ei ole säädetty oikein. Palvelimen virheloki sisältää lisätietoja. Viestiä ei lähetetty seuraaville henkilöille:';

$string['uploadcsvusersaddedsuccessfully'] = 'Tiedostossa olevat käyttäjät on lisätty palveluun onnistuneesti';

$string['uploadleap2afile'] = 'Tuo Leap2A -tiedosto';

$string['uptodate'] = 'ajan tasalla';

$string['useradded'] = 'Käyttäjä lisätty';

$string['usercreationmethod'] = '1 - Käyttäjän luontitapa';

$string['userdeletedsuccessfully'] = 'Käyttäjän poistaminen suoritettu';

$string['usereditdescription'] = 'Tässä voit katsoa ja asettaa henkilötietoja tälle käyttäjätilille. Alla voit myös <a href="#suspend">lukita tai poistaa tämän käyttäjätilin</a>, tai muuttaa tämän käyttäjän asetuksia <a href="#institutions">niihin instituutioihin, joissa he ovat</a>.';

$string['usereditwarning'] = 'NOTE: Saving the account changes will cause the user to be logged out (if currently logged in)';

$string['usernamechangenotallowed'] = 'The chosen authentication method does not allow changes to the username.';

$string['usersallowedmultipleinstitutions'] = 'Käyttäjät voivat olla jäseniä useissa instituutioissa';

$string['usersallowedmultipleinstitutionsdescription'] = 'Jos asetus on päällä, käyttäjät voivat olla jäseniä useissa instituutioissa samaan aikaan';

$string['userscanchooseviewthemes'] = 'Käyttäjät voivat valita sivujen teemat';

$string['userscanchooseviewthemesdescription'] = 'Jos valittu, käyttäjät voivat itse valita haluamansa teeman sivuilleen';

$string['userscanhiderealnames'] = 'Käyttäjä voi piilottaa oikean nimensä';

$string['userscanhiderealnamesdescription'] = 'Jos valittu, käyttäjät voivat hakea toisiaan vain nimimerkillä, jos sellainen on asetettu (käyttäjien hallinnassa haku oikealla nimellä on aina käytössä).';

$string['usersdeletedsuccessfully'] = 'Käyttäjien poistaminen onnistui';

$string['usersearch'] = 'Käyttäjähaku';

$string['usersearchdescription'] = 'Etsi kaikki käyttäjät ja suorita heitä koskevia hallinnollisia toimenpiteitä';

$string['usersearchinstructions'] = 'Voit etsiä käyttäjiä klikkaamalla heidän etu- ja sukunimen etukirjaimia tai kirjoittamalla nimen Kysely-laatiikkoon. Voit myös kirjoittaa sähköpostiosoitteen Kysely-laatikkoon mikäli haluat etsiä sähköpostiosoitteita.';

$string['usersettingslegend'] = 'Käyttäjäasetukset';

$string['usersrequested'] = 'Käyttäjät, jotka ovat pyytäneet jäsenyyttä';

$string['usersseenewthemeonlogin'] = 'Muut käyttäjät näkevät uuden teeman seuraavan kirjoittautumisen yhteydessä.';

$string['userstatstabletitle'] = 'Päivittäiset käyttäjätilastot';

$string['userstoaddorreject'] = 'Käyttäjät, jotka lisätään/hylätään';

$string['userstobeadded'] = 'Käyttäjät, jotka lisätään jäseniksi';

$string['userstobeinvited'] = 'Kutsuttavia käyttäjiä';

$string['userstoberemoved'] = 'Poistolistalla olevat käyttäjät';

$string['userstodisplay'] = 'Käyttäjät, jotka näytetään';

$string['usersunsuspendedsuccessfully'] = 'Käyttäjätilien lukituksen poisto suoritettu';

$string['usersuspended'] = 'Käyttäjätili lukittu';

$string['userunsuspended'] = 'Käyttäjätili uudelleen avattu';

$string['userwillreceiveemailandhastochangepassword'] = 'Käyttäjät saavat sähköpostiviestin, jossa kerrotaan heidän uuden käyttäjätilin asetuksista. Uudet käyttäjät joutuvat vaihtamaan salasanansa ensimmäisen kirjautumisen yhteydessä.';

$string['viewfullsitestatistics'] = 'Näytä kaikki tilastot';

$string['viewsbytype'] = 'Sivuja tyypin mukaan';

$string['viewsperuser'] = 'Käyttäjillä on keskimäärin %s sivua';

$string['viewstatstabletitle'] = 'Katsotuimmat sivut';

$string['viruschecking'] = 'Virustarkistus';

$string['viruscheckingdescription'] = 'Jos asetus on päällä, kaikki palveluun tuodut tiedostot virustarkistetaan ClamAV -ohjelmalla.';

$string['warnings'] = 'Varoitus';

$string['whocancreategroups'] = 'Kuka voi luoda ryhmiä';

$string['whocancreategroupsdescription'] = 'Ketkä näistä käyttäjistä saavat luoda uusia ryhmiä';

$string['whocancreatepublicgroups'] = 'Käyttäjät, jotka saavat luoda julkisia ryhmiä';

$string['whocancreatepublicgroupsdescription'] = 'Käyttäjät, jotka saavat luoda julkisia ryhmiä';

$string['wwwroot'] = 'www-juurihakemisto';

$string['wwwrootdescription'] = 'Tämän Mahara-palvelun http- ja https-URLit';

$string['wysiwyg'] = 'HTML-editori';

$string['wysiwygdescription'] = 'Määrittelee, voivatko käyttäjät valita html-editorin käytön';

$string['wysiwyguserdefined'] = 'Käyttäjien määriteltävissä';

$string['youcannotadministerthisuser'] = 'Et voi hallinnoida tätä käyttäjää';

$string['youcanupgrade'] = 'Voit päivittää Maharan versioista %s (%s) versioon %s (%s)!';

$string['youraverageuser'] = 'Käyttäjällä keskimäärin...';

