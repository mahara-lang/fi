<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['Done'] = 'Tehty';

$string['Export'] = 'Vie portfolio';

$string['Starting'] = 'Aloitus';

$string['allmydata'] = 'Kaikki tietoni';

$string['chooseanexportformat'] = 'Valitse siirtoformaatti';

$string['clicktopreview'] = 'Klikkaa esikatselu';

$string['collectionstoexport'] = 'Vietävät sivuston';

$string['creatingzipfile'] = 'Zip-tiedoston luominen';

$string['exportgeneratedsuccessfully'] = 'Vienti onnistui. %sKlikkaa tästä ladataksesi %s';

$string['exportgeneratedsuccessfullyjs'] = 'Vienti onnistui. %sJatka%s';

$string['exportingartefactplugindata'] = 'Viedään tuotosten pluginien tietoja';

$string['exportingartefacts'] = 'Tuotosten vienti';

$string['exportingartefactsprogress'] = 'Tuotosten: %s/%s vienti';

$string['exportingfooter'] = 'Alatunnisteen vienti';

$string['exportingviews'] = 'Viedään sivuja';

$string['exportingviewsprogress'] = 'Viedään sivuja: %s/%s';

$string['exportpagedescription'] = 'Voit luoda portfoliostasi HTML-sivuston tai Leap2A-tiedoston. Siirtotiedostoon tallentuu profiili, tallennettu sisältö ja portfolion tiedot ja sivut. Asetukset eivät tallennu, eivätkä myöskään kaverit tai ryhmät.';

$string['exportyourportfolio'] = 'Vie portfoliosi omalle koneellesi';

$string['generateexport'] = 'Luo siirtotiedosto';

$string['justsomecollections'] = 'Vain osa sivustoista';

$string['justsomeviews'] = 'Joitakin sivuistani';

$string['noexportpluginsenabled'] = 'Pääkäyttäjä ei ole sallinnut siirtoplugineja, joten et voi käyttää tätä toimintoa';

$string['nonexistentfile'] = "Tiedostoa '%s' ei löydy";

$string['pleasewaitwhileyourexportisbeinggenerated'] = 'Odota, kunnes vienti on valmis...';

$string['reverseselection'] = 'Käännä valinnat';

$string['selectall'] = 'Valitse kaikki';

$string['setupcomplete'] = 'Asennus valmis';

$string['unabletoexportportfoliousingoptions'] = 'Portfolion vienti epäonnistui valituilla asetuksilla';

$string['unabletogenerateexport'] = 'Vienti epäonnistui';

$string['viewstoexport'] = 'Sivut, jotka viedään';

$string['whatdoyouwanttoexport'] = 'Mitä haluat viedä?';

$string['writingfiles'] = 'Tiedostojen kirjoitus';

$string['youarehere'] = 'Olet tässä';

$string['youmustselectatleastonecollectiontoexport'] = 'Sinun tulee valita vähintään yksi sivusto vientiä varten';

$string['youmustselectatleastoneviewtoexport'] = 'Sinun tulee valita vähintään yksi sivu vientiä varten';

$string['zipnotinstalled'] = 'Järjestelmässäsi ei ole zip-komentoa. Asenna zip mahdollistaaksesi tämän toiminnon';

