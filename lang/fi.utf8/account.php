<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['accountdeleted'] = 'Käyttäjätilisi on poistettu.';

$string['accountoptionsdesc'] = 'Yleiset käyttäjätilin asetukset';

$string['changepassworddesc'] = 'Uusi salasana';

$string['changepasswordotherinterface'] = 'Voit  <a href="%s">vaihtaa salasanasi</a> toisen käyttöliittymän kautta';

$string['changeusername'] = 'Uusi käyttäjätunnus';

$string['changeusernamedesc'] = 'Käyttäjätunnus, jolla kirjaudut palveluun. Nimi on 3-30 merkkiä pitkä, voi sisältää kirjaimia, numeroita ja useimpia erikoismerkkejä, mutta ei välilyöntejä.';

$string['changeusernameheading'] = 'Vaihda käyttäjätunnuksesi';

$string['deleteaccount'] = 'Poista käyttäjätili';

$string['deleteaccountdescription'] = 'Mikäli poistat käyttäjätilisi, muut käyttäjät eivät pääse enää katsomaan profiilisi tietoja eikä näkymiäsi. Tekemäsi merkinnät eri foorumeihin jäävät näkyviin, mutta kirjoittajan nimeä ei enää näytetä.';

$string['disabled'] = 'Pois käytöstä';

$string['disablemultipleblogserror'] = 'Et voi poistaa useita blogeja käytöstä, jos sinulla on useampi kuin yksi.';

$string['enabled'] = 'Käytössä';

$string['enablemultipleblogs'] = 'Kirjoita useita blogeja';

$string['enablemultipleblogsdescription'] = 'Oletuksena sinulla on yksi blogi. Jos haluat kirjoittaa useita, valitse tämä.';

$string['friendsauth'] = 'Kaveriksi tuleminen vaatii hyväksyntäni';

$string['friendsauto'] = 'Kaveripyynnöt hyväksytään automaattisesti';

$string['friendsdescr'] = 'Kaverien hallinta';

$string['friendsnobody'] = 'Minua ei voi valita kaveriksi';

$string['hiderealname'] = 'Piilota oikea nimi';

$string['hiderealnamedescription'] = 'Valitse tämä, jos et halua, että oikea nimesi löytyy käyttäjähaulla.';

$string['language'] = 'Kieli';

$string['maildisabled'] = 'Sähköposti pois käytöstä';

$string['maildisableddescription'] = 'Sähköpostin lähettäminen osoitteeseesi on pois käytöstä. Voit ottaa lähettämisen käyttöön uudelleen <a href="%s">asetussivulla</a>.';

$string['messagesallow'] = 'Kaikki käyttäjät voivat lähettää minulle viestejä';

$string['messagesdescr'] = 'Viestit muilta käyttäjiltä';

$string['messagesfriends'] = 'Vain kaverilistallani olevat käyttäjät voivat lähettää minulle viestejä';

$string['messagesnobody'] = 'En halua vastaanottaa viestejä keneltäkään';

$string['mobileuploadtoken'] = 'Mobiililatausavain';

$string['mobileuploadtokendescription'] = 'Syötä sama latausavain tähän ja Android-puhelimeesi.
Avain vaihtuu jokaisen tiedostolatauksen yhteydessä. Jos latauksessa on 
ongelmia, voit vaihtaa avaimen.<br /><br />
Alla olevasta QR-koodista voit skannata asetukset puhelimeesi. Muista tallentaa muutokset ensin.
';

$string['off'] = 'Pois päältä';

$string['oldpasswordincorrect'] = 'Tämä ei ole nykyinen salasanasi';

$string['on'] = 'Päällä';

$string['prefsnotsaved'] = 'Asetuksiesi tallentaminen epäonnistui!';

$string['prefssaved'] = 'Asetuksesi tallennettu';

$string['showhomeinfo'] = 'Näytä tietoja palvelusta työpöydällä';

$string['showviewcolumns'] = 'Näytä toiminto, joilla voi lisätä tai poistaa sarakkeita näkymän muokkaustilassa';

$string['tagssideblockmaxtags'] = 'Avainsanojen maksimimäärä pilvessä';

$string['tagssideblockmaxtagsdescription'] = 'Avainsanapilvessä näytettävien avainsanojen maksimimäärä';

$string['updatedfriendcontrolsetting'] = 'Ystävien hallintaa koskevat asetukset  päivitetty';

$string['usernameexists'] = 'Käyttäjätunnus on varattu. Valitse toinen.';

$string['wysiwygdescr'] = 'HTML-editori';

