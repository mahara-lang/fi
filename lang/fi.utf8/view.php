<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['100'] = $string['50,50'] = $string['33,33,33'] = $string['25,25,25,25'] = $string['20,20,20,20,20'] = 'sama leveys';

$string['15,70,15'] = 'paljon isompi keskisarake';

$string['20,30,30,20'] = 'isommat keskisarakkeet';

$string['25,50,25'] = 'isompi keskisarake';

$string['33,67'] = 'isompi oikea sarake';

$string['67,33'] = 'isompi vasen sarake';

$string['Added'] = 'Lisätty';

$string['Browse'] = 'Selaa';

$string['Configure'] = 'Määritä asetukset';

$string['Owner'] = 'Omistaja';

$string['Preview'] = 'Esikatselu';

$string['Search'] = 'Etsi';

$string['Submitted'] = 'Jätetty arvioitavaksi';

$string['Template'] = 'Pohja';

$string['Untitled'] = 'Nimetön';

$string['View'] = 'Sivu';

$string['Views'] = 'Sivut';

$string['access'] = 'Pääsy';

$string['accessbetweendates2'] = 'Kukaan muu ei voi nähdä tätä sivua ennen %s tai %s jälkeen';

$string['accessdates'] = 'Aikaväli';

$string['accessfromdate2'] = 'Kukaan muu ei voi nähdä tätä sivua ennen %s';

$string['accesslist'] = 'Annetut oikeudet';

$string['accessuntildate2'] = 'Kukaan muu ei voi nähdä tätä sivua %s jälkeen';

$string['add'] = 'Lisää';

$string['addcolumn'] = 'Lisää sarake';

$string['addedtowatchlist'] = 'Sivu lisättiin seurantalistallesi';

$string['addnewblockhere'] = 'Lisää uusi alue tähän';

$string['addtowatchlist'] = 'Lisää sivu seurantalistalle';

$string['addtutors'] = 'Lisää tuutorit';

$string['addusertogroup'] = 'Lisää tämä käyttäjä ryhmään';

$string['allowcommentsonview'] = 'Jos valittu, käyttäjät voivat kommentoida sivuja.';

$string['allowcopying'] = 'Salli kopiointi';

$string['allviews'] = 'Kaikki sivut';

$string['alreadyinwatchlist'] = 'Sivu on jo seurantalistallasi';

$string['artefacts'] = 'Tuotokset';

$string['artefactsinthisview'] = 'Tuotokset tällä sivulla';

$string['attachedfileaddedtofolder'] = 'Liitetty tiedosto %s on lisätty kansioosi\'%s\'.';

$string['attachment'] = 'Liitetiedosto';

$string['back'] = 'Takaisin';

$string['blockconfigurationrenderingerror'] = 'Asetusten päivittäminen epäonnistui, koska lohkoa ei voi näyttää.';

$string['blockcopypermission'] = 'Estä kopiointilupa';

$string['blockcopypermissiondesc'] = 'Jos sallit muiden käyttäjien kopioida tämän sivun, voit valita miten tämä alue kopioidaan';

$string['blockinstanceconfiguredsuccessfully'] = 'Alueen asetukset päivitetty';

$string['blocksinstructionajax'] = 'Alla näet, miltä sivusi tulee näyttämään.<br>Raahaa sisältölohkoja merkitylle alueelle lisätäksesi ne sivupohjaan. Sivupohjassa voit raahaamalla liikuttaa alueita asemoidaksesi ne.';

$string['blocksintructionnoajax'] = 'Valitse alue ja päätä mihin lisäät sen sivullasi. Voit asemoida alueen käyttämällä nuolipainikkeita sen otsikkokentässä.';

$string['blocktitle'] = 'Alueen otsikko';

$string['blocktypecategory.external'] = 'Ulkoiset lähteet';

$string['blocktypecategory.fileimagevideo'] = 'Tiedostot, kuva ja video';

$string['blocktypecategory.general'] = 'Yleinen';

$string['by'] = 'käyttäjältä';

$string['cantaddcolumn'] = 'sivulle ei voi lisätä sarakkeita';

$string['cantdeleteview'] = 'Et voi poistaa tätä sivua';

$string['canteditdontown'] = 'Et voi muokata tätä sivua, koska et omista sitä';

$string['canteditsubmitted'] = 'Et voi muokata tätä sivua koska se on lähetetty ryhmään "%s" arvioutavaksi. Sinun täytyy odottaa kunnes tuutori kuittaa sivusi.';

$string['cantremovecolumn'] = 'Sivulla tulee olla vähintään yksi sarake';

$string['cantsubmitviewtogroup'] = 'Et voi lähettää tätä sivua arvioittavaksi tälle ryhmälle';

$string['changecolumnlayoutfailed'] = 'Sivupohjan tallennus epäonnistui. Toinen käyttäjä on mahdollisesti muokannut sitä samaan aikaan. Yritä myöhemmin uudelleen.';

$string['changemyviewlayout'] = 'Muuta sivupohjaa';

$string['changeviewlayout'] = 'Muokkaa sivun palstoja';

$string['changeviewtheme'] = 'Valitsemaasi teemaa ei voi enää käyttää. Ole hyvä ja valitse toinen.';

$string['choosetemplategrouppagedescription'] = '<p>Etsi sivuja, joita tällä ryhmällä on oikeus kopioida. Voit katsoa sivua klikkaamalla sen nimeä.
    Kun löydät sivun, jonka haluat kopioida, klikkaa "Kopioi sivu" -painiketta tehdäksesi kopion ja aloittaaksesi sen räätälöinnin.</p>
    <p><strong>Huomio:</strong> Ryhmät eivät voi tällä hetkellä tehdä kopioita blogeista tai blogimerkinnöistä.</p>';

$string['choosetemplateinstitutionpagedescription'] = '<p>Etsi sivuja, joita tällä instituutiolla on oikeus kopioida. Voit katsoa sivua klikkaamalla sen nimeä.
    Kun löydät sivun, jonka haluat kopioida, klikkaa "Kopioi sivu" -painiketta tehdäksesi kopion ja aloittaaksesi sen räätälöinnin.</p>
    <p><strong>Huomio:</strong> Instituutiot eivät voi tällä hetkellä tehdä kopioita blogeista tai blogimerkinnöistä.</p>';

$string['choosetemplatepagedescription'] = '<p>Etsi sivuja, joita sinulla on oikeus kopioida. Voit katsoa sivua klikkaamalla sen nimeä.
    Kun löydät sivun, jonka haluat kopioida, klikkaa "Kopioi sivu" -painiketta tehdäksesi kopion ja aloittaaksesi sen räätälöinnin. </p>';

$string['clickformoreinformation'] = 'Klikkaa saadaksesi lisätietoja tai lähettääksesi palautetta';

$string['column'] = 'sarake';

$string['columns'] = 'saraketta';

$string['complaint'] = 'Reklamaatio';

$string['configureblock'] = 'Muokkaa %s alue';

$string['configurethisblock'] = 'Muokkaa tämä alue';

$string['confirmcancelcreatingview'] = 'Tämä sivun rakentaminen on vielä kesken. Haluatko todella keskeyttää?';

$string['confirmdeleteblockinstance'] = 'Haluatko todella poistaa tämän alueen?';

$string['copiedblocksandartefactsfromtemplate'] = 'Kopioitu %d aluetta ja %s tuotosta sivulta %s';

$string['copyaview'] = 'Kopioi sivu';

$string['copyfornewgroups'] = 'Kopioi uusille ryhmille';

$string['copyfornewgroupsdescription'] = 'Tee kopio tästä sivusta kaikkiin uusiin ryhmiin jotka ovat tyypiltään:';

$string['copyfornewmembers'] = 'Kopioi uusille instituution jäsenille';

$string['copyfornewmembersdescription'] = 'Tee automaattisesti henkilökohtainen kopio tästä sivusta instituutio %s:n kaikkille uusille jäsenille';

$string['copyfornewusers'] = 'Kopioi uusille käyttäjille';

$string['copyfornewusersdescription'] = 'Aina, kun uusi käyttäjä luodaan, tee automaattisesti kopio tästä sivusta käyttäjän portfolioon.';

$string['copynewusergroupneedsloggedinaccess'] = 'Kaikilla kirjautuneilla käyttäjillä tulee olla oikeus nähdä sivut, jotka kopioidaan uusille käyttäjille tai ryhmille.';

$string['copythisview'] = 'Kopioi tämä sivu';

$string['copyview'] = 'Kopioi sivu';

$string['createemptyview'] = 'Luo tyhjä sivu';

$string['createview'] = 'Luo sivu';

$string['dashboard'] = 'Työpöytä';

$string['dashboardviewtitle'] = 'Työpöytä';

$string['date'] = 'Päiväys';

$string['deletespecifiedview'] = 'Poista sivu "%s"';

$string['deletethisview'] = 'Poista tämä sivu';

$string['deleteviewconfirm'] = 'Haluatko todella poistaa tämän sivun? Poistoa ei voi peruuttaa.<br/>Voit tehdä varmuuskopion sivusta <a href="%sexport/" target="_blank">täällä</a>.';

$string['deleteviewconfirmnote'] = '<p><strong>Huomio:</strong> sivulle liitettyä sisältöä ei poisteta. Kaikki sivun kommentit ja tekstikentät kuitenkin poistetaan.</p>';

$string['description'] = 'Sivun kuvaus';

$string['displayview'] = 'Näytä sivu';

$string['editaccess'] = 'Anna katseluoikeuksia';

$string['editaccessinvalidviewset'] = 'Et voi muokata näiden sivujen tai kokoelmien julkaisusääntöjä';

$string['editaccesspagedescription3'] = 'Oletuksena sivuja ei näytetä muille. Voit sallia pääsyn sivuillesi julkaisusäännöillä. Muista tallentaa asetukset sivun alalaidassa olevalla painikkeella.';

$string['editblockspagedescription'] = '<p>Vedä sisältölohkoja yllä olevilta välilehdiltä sivullesi.</p>';

$string['editcontent'] = 'Muokkaa sisältöä';

$string['editcontentandlayout'] = 'Muokkaa rakennetta ja sisältöä';

$string['editlayout'] = 'Muokkaa asettelua';

$string['editsecreturlaccess'] = 'Muokkaa pääsyä salaisella osoitteella';

$string['editthisview'] = 'Muokkaa tätä sivua';

$string['edittitle'] = 'Muokkaa otsikkoa';

$string['edittitleanddescription'] = 'Muokkaa otsikkoa ja kuvausta';

$string['empty_block'] = 'Valitse vasemmalla olevasta puusta tuotos, jonka haluat laittaa tähän';

$string['emptylabel'] = 'Klikkaa tähän kirjoittaaksesi tekstiä kuvauskenttään ???';

$string['err.addblocktype'] = 'Aluetta ei voitu lisätä sivulle';

$string['err.addcolumn'] = 'Uuden sarakkeen lisääminen epäonnistui';

$string['err.changetheme'] = 'Teeman muuttaminen epäonnistui';

$string['err.moveblockinstance'] = 'Aluetta ei voitu siirtää valittuun paikkaan';

$string['err.removeblockinstance'] = 'Aluetta ei voitu poistaa';

$string['err.removecolumn'] = 'Sarakkeen poistaminen epäonnistui';

$string['everyoneingroup'] = 'Kaikki ryhmässä';

$string['filescopiedfromviewtemplate'] = 'Tiedostot kopioitu sivulta %s';

$string['forassessment'] = '';

$string['friend'] = 'Kaveri';

$string['friends'] = 'Kaverit';

$string['generatesecreturl'] = 'Luo uusi salainen osoite sivulle %s';

$string['grouphomepage'] = 'Ryhmän aloitussivu';

$string['grouphomepagedescription'] = 'Ryhmän aloitussivu näytetään Ryhmän tiedot -sivulla';

$string['grouphomepageviewtitle'] = 'Ryhmän aloitussivu';

$string['groups'] = 'Ryhmät';

$string['groupviews'] = 'Ryhmän sivut';

$string['in'] = '-';

$string['institutionviews'] = 'Instituution sivut';

$string['invalidcolumn'] = 'Sarake %s ei mahdu';

$string['inviteusertojoingroup'] = 'Kutsu tämä käyttäjä ryhmän jäseneksi';

$string['listviews'] = 'Listaa sivut';

$string['loggedin'] = 'Kirjautuneet käyttäjät';

$string['moreoptions'] = 'Lisävalinnat';

$string['moveblockdown'] = 'Siirrä %s alue alas';

$string['moveblockleft'] = 'Siirrä %s alue vasemmalle';

$string['moveblockright'] = 'Siirrä %s alue oikealle';

$string['moveblockup'] = 'Siirrä %s alue ylös';

$string['movethisblockdown'] = 'Siirrä tämä alue alas';

$string['movethisblockleft'] = 'Siirrä tämä alue vasemmalle';

$string['movethisblockright'] = 'Siirrä tämä alue oikealle';

$string['movethisblockup'] = 'Siirrä tämä alue ylös';

$string['newsecreturl'] = 'Uusi salainen osoite';

$string['newstartdatemustbebeforestopdate'] = 'Alkamisaika ei voi olla päättymisajan jälkeen';

$string['newstopdatecannotbeinpast'] = 'Päättymisaika ei voi olla menneisyydessä';

$string['next'] = 'Seuraava';

$string['noaccesstoview'] = 'Sinulla ei oikeutta nähdä tätä sivua';

$string['noartefactstochoosefrom'] = 'Tuotosta ei ole valittavissa';

$string['noblocks'] = 'Tässä kategoriassa ei ole alueita :(';

$string['nobodycanseethisview2'] = 'Sivua ei näytetä muille käyttäjille';

$string['nocopyableviewsfound'] = 'Ei kopioitavia sivuja';

$string['noownersfound'] = 'Omistajia ei löytynyt';

$string['notifysiteadministrator'] = 'Lähetä ilmoitus palvelun pääkäyttäjälle';

$string['notifysiteadministratorconfirm'] = 'Oletko varma, että sivu sisältää sopimatonta materiaalia?';

$string['notitle'] = 'Otsikkoa ei ole määritelty';

$string['notobjectionable'] = 'Sisältö kunnossa';

$string['noviewlayouts'] = '%s sarakkeen sivulle ei ole sivupohjaa';

$string['noviews'] = 'Ei sivuja.';

$string['numberofcolumns'] = 'Sarakkeiden lukumäärä';

$string['otherusersandgroups'] = 'Näytä toisille käyttäjille ja ryhmille';

$string['overridingstartstopdate'] = 'Alkamis- ja päättymispäivät';

$string['overridingstartstopdatesdescription'] = 'Annetut oikeudet ovat voimassa vain alkamispäivän jälkeen ja ennen päättymispäivää.';

$string['owner'] = 'omistaja';

$string['ownerformat'] = 'Nimen esitysmuoto';

$string['ownerformatdescription'] = 'Miten haluat, että nimesi näytetään?';

$string['owners'] = 'omistajat';

$string['portfolio'] = 'Portfolio';

$string['print'] = 'Tulosta';

$string['profile'] = 'Profiili';

$string['profileicon'] = 'Profiilikuva';

$string['profileviewtitle'] = 'Profiilisivu';

$string['public'] = 'Julkinen';

$string['reallyaddaccesstoemptyview'] = 'Sivullasi ei ole sisältöä. Haluatko todella julkaista sen näille käyttäjille?';

$string['reallydeletesecreturl'] = 'Haluatko varmasti poistaa tämän osoitteen?';

$string['remove'] = 'Poista';

$string['removeblock'] = 'Poista alue %s';

$string['removecolumn'] = 'Poista tämä sarake';

$string['removedfromwatchlist'] = 'Sivu poistettiin seurantalistaltasi';

$string['removefromwatchlist'] = 'Poista sivu seurantalistalta';

$string['removethisblock'] = 'Poista tämä alue';

$string['reportobjectionablematerial'] = 'Raportoi sopimattomasta materiaalista';

$string['reportsent'] = 'Raporttisi on lähetetty';

$string['searchowners'] = 'Etsi omistajia';

$string['searchviews'] = 'Etsi sivuja';

$string['searchviewsbyowner'] = 'Etsi sivuja omistajan mukaan';

$string['secreturldeleted'] = 'Salainen osoite poistettiin';

$string['secreturls'] = 'Salaiset osoitteet';

$string['secreturlupdated'] = 'Salainen osoite päivitettiin';

$string['selectaviewtocopy'] = 'Valitse sivu, jonka haluat kopioida:';

$string['share'] = 'Oikeudet';

$string['sharedviews'] = 'Jaetut sivut';

$string['sharedviewsdescription'] = 'Tässä näet viimeksi muokatut tai kommentoidut sivut, joihin sinulla on katseluoikeus. Katseluoikeus voi olla annettu sinulle suoraan, tai se voi tulla kavereiden tai ryhmien kautta.';

$string['shareview'] = 'Muokkaa oikeuksia';

$string['sharewith'] = 'Katseluoikeudet';

$string['sharewithmygroups'] = 'Näytä omille ryhmille';

$string['show'] = 'Näytä';

$string['startdate'] = 'Oikeuksien alkamispäivä';

$string['stopdate'] = 'Oikeuksien päättymispäivä';

$string['submitaviewtogroup'] = 'Jätä sivu arvioitavaksi tähän ryhmään';

$string['submittedforassessment'] = 'Jätetty arvioitavaksi';

$string['submitthisviewto'] = 'Lähetä sivu arvioitavaksi ryhmään';

$string['submitviewconfirm'] = 'Jos lähetät sivun \'%s\' ryhmään \'%s\' arviointia varten, et voi muokata sivua ennen kuin tuutorisi on kuitannut sen. Oletko varma, että haluat lähettää sivun nyt?';

$string['submitviewtogroup'] = 'Lähetä \'%s\' ryhmään \'%s\' arviointia varten';

$string['success.addblocktype'] = 'Alue lisätty';

$string['success.addcolumn'] = 'Sarake lisätty';

$string['success.changetheme'] = 'Teemaa muutettu';

$string['success.moveblockinstance'] = 'Alue siirretty';

$string['success.removeblockinstance'] = 'Alue poistettu';

$string['success.removecolumn'] = 'Sarake poistettu';

$string['tagsonly'] = 'vain avainsanat';

$string['templatedescription'] = 'Salli toisten käyttäjien kopioida sivu';

$string['templatedescriptionplural'] = 'Salli toisten käyttäjien kopioida sivut.';

$string['thisviewmaybecopied'] = 'Kopiointi on sallittu';

$string['timeofsubmission'] = 'Jättöaika';

$string['title'] = 'Sivun otsikko';

$string['titleanddescription'] = 'Otsikko, kuvaus, avainsanat';

$string['token'] = 'Salainen URL';

$string['tutors'] = 'tuutorit';

$string['unrecogniseddateformat'] = 'Tunnistamaton päiväys muoto';

$string['updatedaccessfornumviews'] = 'Katseluoikeudet tallennettiin %d sivulle';

$string['updatewatchlistfailed'] = 'Seurantalistan päivitys epäonnistui';

$string['users'] = 'Käyttäjät';

$string['view'] = 'sivu';

$string['viewaccesseditedsuccessfully'] = 'Oikeudet sivuun tallennettu';

$string['viewcolumnspagedescription'] = 'Valitse ensin sarakkeiden lukumäärä sivulla. Seuraavassa vaiheessa voit vaihtaa sarakkeiden leveyksiä.';

$string['viewcopywouldexceedquota'] = 'Tämän sivun kopiointi ylittää tiedostokiintiösi.';

$string['viewcreatedsuccessfully'] = 'Sivu luotu';

$string['viewdeleted'] = 'Sivu poistettu';

$string['viewfilesdirdesc'] = 'Tiedostot kopioiduista sivuista';

$string['viewfilesdirname'] = 'portfoliosivun tiedostot';

$string['viewinformationsaved'] = 'Sivun tiedot tallennettu';

$string['viewlayoutchanged'] = 'Sivupohja vaihdettu';

$string['viewlayoutpagedescription'] = 'Valitse miten sarakkeet näytetään sivulla.';

$string['viewobjectionableunmark'] = 'Tämän sivun on ilmoitettu sisältävän sopimatonta materiaalia. Jos asia on korjattu, voit poistaa ilmoituksen. Muille ylläpitäjille ilmoitetaan tästä.';

$string['views'] = 'sivua';

$string['viewsavedsuccessfully'] = 'Sivu tallennettu';

$string['viewscopiedfornewgroupsmustbecopyable'] = 'Sinun tulee sallia kopiointi ennen kuin voit asettaa sivun kopioitavaksi uusille ryhmille.';

$string['viewscopiedfornewusersmustbecopyable'] = 'Sinun tulee sallia kopiointi ennen kuin voit asettaa sivun kopioitavaksi uusille käyttäjille.';

$string['viewsownedbygroup'] = 'Ryhmän omistamat sivut';

$string['viewssharedtogroup'] = 'Ryhmälle jaetut sivut';

$string['viewssharedtogroupbyothers'] = 'Toisten ryhmälle jakamat sivut';

$string['viewssubmittedtogroup'] = 'Ryhmään arvioitavaksi lähetetyt sivut';

$string['viewsubmitted'] = 'Sivu lähetetty';

$string['viewsubmittedtogroup'] = 'Tämä sivu on lähetetty ryhmään <a href="%s">%s</a>';

$string['viewsubmittedtogroupon'] = 'Sivu on jätetty arvioitavaksi ryhmään <a href="%s">%s</a> %s';

$string['viewtitleby'] = '%s &mdash; <a href="%s">%s</a>';

$string['viewunobjectionablebody'] = '%s on tarkastanut käyttäjän %3$s sivun %2$s ja todennut, ettei se sisällä mitään käyttöehtojen vastaista';

$string['viewunobjectionablesubject'] = 'Käyttäjä %2$s totesi sivunn %1$s sisällön olevan käyttöehtojen mukaista';

$string['viewvisitcount'] = 'Sivua on katsottu %d kertaa välillä %s - %s';

$string['watchlistupdated'] = 'Seurantalistasi on päivitetty';

$string['whocanseethisview'] = 'Tämän sivun saa nähdä';

$string['youhavenoviews'] = 'Sinulla ei ole sivuja.';

$string['youhaventcreatedanyviewsyet'] = 'Et ole luonut vielä yhtään sivua.';

$string['youhaveoneview'] = 'Sinulla on 1 sivu.';

$string['youhavesubmitted'] = 'Olet jättäny sivun <a href="%s">%s</a> arvioitavaksi tähän ryhmään';

$string['youhavesubmittedon'] = 'Jätit sivunn <a href="%s">%s</a> arvioitavaksi tähän ryhmään %s';

$string['youhaveviews'] = 'Sinulla on %s sivua.';

