<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['Collection'] = 'Sivusto';

$string['Collections'] = 'Sivustot';

$string['about'] = 'Tietoja';

$string['access'] = 'Oikeudet';

$string['accesscantbeused'] = 'Oikeuksia ei voitu tallentaa. Sivun salaista osoitetta ei voi käyttää useiden sivujen kanssa.';

$string['accessignored'] = 'Joitain salaisia osoitteita ei otettu huomioon tallennettaessa.';

$string['accessoverride'] = 'Oikeuksien ohitus';

$string['accesssaved'] = 'Sivuston oikeudet tallennettiin.';

$string['add'] = 'Lisää';

$string['addviews'] = 'Lisää sivuja';

$string['addviewstocollection'] = 'Lisää sivuja sivustoon';

$string['back'] = 'Takaisin';

$string['cantdeletecollection'] = 'et voi poistaa tätä sivustoa';

$string['canteditdontown'] = 'Et voi muokata sivustoa, jota et omista.';

$string['collection'] = 'sivusto';

$string['collectionaccess'] = 'Oikeudet sivustoon';

$string['collectionaccesseditedsuccessfully'] = 'Oikeudet sivustoon tallennettiin';

$string['collectionconfirmdelete'] = 'Tämän sivuston sivuja ei poisteta. Oletko varma, että haluat poistaa sivuston?';

$string['collectioncreatedsuccessfully'] = 'Sivusto luotu.';

$string['collectioncreatedsuccessfullyshare'] = 'Sivusto luotu. Määrittele seuraavaksi katseluoikeudet sivustoon.';

$string['collectiondeleted'] = 'Sivusto poistettu.';

$string['collectiondescription'] = 'Sivusto on joukko sivuja, jotka on linkitetty toisiinsa ja joihin on pääsy samoilla oikeuksilla. Voit luoda niin monta sivustoa kuin haluat, mutta kukin sivu voi olla vain yhdessä sivustossa.';

$string['collectioneditaccess'] = 'Muokkaat oikeuksia %d:n sivuun sivustossa';

$string['collections'] = 'sivustoa';

$string['collectionsaved'] = 'Sivusto tallennettu.';

$string['confirmcancelcreatingcollection'] = 'Tämä sivusto ei ole valmis. Haluatko varmasti peruuttaa?';

$string['created'] = 'Luotu';

$string['deletecollection'] = 'Poista sivusto';

$string['deletespecifiedcollection'] = 'Poista sivusto \'%s\'';

$string['deleteview'] = 'Poista sivu sivustosta';

$string['deletingcollection'] = 'Poistetaan sivusto';

$string['description'] = 'Sivuston kuvaus';

$string['editaccess'] = 'Muokkaa oikeuksia sivustoon';

$string['editcollection'] = 'Muokkaa sivustoa';

$string['editingcollection'] = 'Muokataan sivustoa';

$string['edittitleanddesc'] = 'Muokkaa otsikkoa ja kuvausta';

$string['editviewaccess'] = 'Muokkaa oikeuksia sivuun';

$string['editviews'] = 'Muokkaa sivuston sivuja';

$string['emptycollection'] = 'Tyhjä sivusto';

$string['emptycollectionnoeditaccess'] = 'Et voi muokata tyhjän sivuston oikeuksia. Lisää ensin sivuja.';

$string['manageviews'] = 'Hallitse sivuja';

$string['name'] = 'Sivuston nimi';

$string['newcollection'] = 'Uusi sivusto';

$string['nocollectionsaddone'] = 'Ei vielä sivustoja. %sLisää uusi%s!';

$string['nooverride'] = 'Ei oikeuksien ohitusta';

$string['noviews'] = 'Ei sivuja.';

$string['noviewsaddsome'] = 'Ei vielä sivuja sivustossa. %sLisää sivu%s';

$string['noviewsavailable'] = 'Ei lisättäviä sivuja.';

$string['overrideaccess'] = 'Oikeuksien ohitus';

$string['pluginname'] = 'Collections';

$string['potentialviews'] = 'Mahdolliset sivut';

$string['saveapply'] = 'Käytä ja tallenna';

$string['savecollection'] = 'Tallenna sivusto';

$string['update'] = 'Päivitä';

$string['usecollectionname'] = 'Käytä sivuston nimeä?';

$string['usecollectionnamedesc'] = 'Valitse tämä, jos haluat käyttää sivuston nimeä lohkon otsikon sijaan.';

$string['viewaddedtocollection'] = 'Sivu lisättiin sivustoon. Uuden sivun katseluoikeudet lisättiin sivuston oikeuksiin.';

$string['viewcollection'] = 'Näytä sivuston tiedot';

$string['viewconfirmremove'] = 'Haluatko varmasti poistaa sivuston sivun?';

$string['viewcount'] = 'Sivua';

$string['viewnavigation'] = 'Näytä navigointipalkki';

$string['viewnavigationdesc'] = 'Lisää navigointipalkki oletuksena kaikkiin sivuston sivuihin.';

$string['viewremovedsuccessfully'] = 'Sivu poistettiin.';

$string['viewsaddedtocollection'] = 'Sivut lisättiin sivustoon. Uusien sivujen katseluoikeudet lisättiin sivuston oikeuksiin.';

$string['viewstobeadded'] = 'Lisättävät sivut';

